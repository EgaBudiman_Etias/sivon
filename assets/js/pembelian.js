function dk_beli_clear()
{
	var $table = $('.dk-beli-table');
	
	$table.find('.select2').val('').trigger('change');
	$table.find('input[type=text]').val('');
	
	$table.find('tbody tr').not(':first').remove();
}
function getSaldo(){
    var pmbl_akun_id=$('#pmbl_akun_id').val();
    console.log(pmbl_akun_id);
    if(pmbl_akun_id!='0'){
        $.post (
            site_url+'/master/akun/ajax_akun_nominal'
            , { akun_id: pmbl_akun_id }
            , function(msg) {
                $('#saldo_rdn').val(msg.nominal);
            }
        );
    }else{
        $('#saldo_rdn').val('0');
    }   
    
}
function get_fee_pembelian(pmbl_akun_id){
    var $jumlah_lot = $('.dk-beli-jumlah-lot');
    
    $.post (
        site_url+'/master/akun/ajax_akun'
        , { pmbl_akun_id: pmbl_akun_id }
        , function(response) {
           return response;
        }
    );
}   

function dk_hitung_bruto()
{   
    var $sahamx=$('.dk-beli-saham');
	var $jumlah_lotx = $('.dk-beli-jumlah-lot');
    var $jumlah_sharesx = $('.dk-beli-jumlah-shares');
    var $harga_sharesx = $('.dk-beli-harga-shares');
    var $feex = $('.dk-beli-fee-pembelian');
    var $brutox = $('.dk-beli-bruto');
    var $feerupiahx = $('.dk-beli-fee-pembelian-rp');
    var $nettox = $('.dk-beli-netto');
    var netto=0 ;
    var bruto=0;
    var jumlah_shares=0;
    var feerupiah=0;
    var total=0;
    
    $.each ($sahamx, function(i, o) {
        var lot = parseInt($($jumlah_lotx[i]).val());
        var harga = parseInt($($harga_sharesx[i]).val());
        var feepersen = parseFloat($($feex[i]).val());
        
        if (isNaN(lot)) {
			lot = 0;
		}
		
		if (isNaN(harga)) {
			harga = 0;
		}

        if (isNaN(feepersen)) {
			feepersen = 0;
		}
        jumlah_shares=lot*100;
        $($jumlah_sharesx[i]).val(jumlah_shares);

        bruto=lot*100*harga;
        $($brutox[i]).val(bruto);

        feerupiah=bruto*feepersen/100;
        $($feerupiahx[i]).val(feerupiah);

        netto=bruto+feerupiah;
        $($nettox[i]).val(netto);

        total = total + netto;

    });
	$('.dk-beli-total-harga').val(total);
	
}
$().ready(function() {
    $('.dk-beli-table').on('keyup', '.dk-beli-jumlah-lot', dk_hitung_bruto);
    $('.dk-beli-table').on('keyup', '.dk-beli-harga-shares', dk_hitung_bruto);
    $('.dk-beli-table').on('change', '.dk-beli-saham', function() {
        var pmbl_akun_id=$('#pmbl_akun_id').val();
        var sham_id=$(this).val();
        var $row = $(this).closest('tr');
        var is_sama = false;
        var $this = $(this);
        var $jumlah_lot = $('.dk-beli-jumlah-lot');
        
        if(sham_id != ''){
            if(pmbl_akun_id==""){
                alert("Pilih Akun Terlebih Dahulu");
                
                return;  
            };
            $('.dk-beli-saham').not(this).each(function() {
				if (sham_id == $(this).val()) {
					is_sama = true;
				}
			});
            
            if (is_sama) {
				alert('Saham sudah dipilih sebelumnya!');
				
				$this.val('').trigger('change');
				$row.find('input[type=text]').val('');
				return;
			}
            $.post (
                site_url+'/master/akun/ajax_akun'
                , { pmbl_akun_id: pmbl_akun_id,akun_id: pmbl_akun_id ,sham_id:sham_id }
                , function(response) {
                    console.log(response.saldosaham);
                    $row.find('.dk-beli-fee-pembelian').val(response.seku_broker_fee_pembelian);
                    $row.find('.dk-beli-jumlah-lot-saat-ini').val(response.saldosaham);
                    $row.find('.dk-beli-jumlah-lot').val(1);
                    $row.find('.dk-beli-harga-shares').val(1);
                    dk_hitung_bruto();
                }
            );
            
        }else{
            $row.find('.dk-beli-fee-pembelian').val(0);
            $row.find('.dk-beli-jumlah-lot').val(0);
            $row.find('.dk-beli-jumlah-lot-saat-ini').val(0);
            // $row.find('.dk-beli-jumlah-shares').val(100);
            $row.find('.dk-beli-harga-shares').val(0);
            dk_hitung_bruto();
        }
        
    });
    $('#pmbl_akun_id').on('change',function(){
        dk_beli_clear(); 
        var pmbl_akun_id=$('#pmbl_akun_id').val();
        $('#pmbl_akun_id2').val(pmbl_akun_id);
        var $row = $('.dk-beli-table').closest('tr');
        $row.find('.dk-beli-fee-pembelian').val('');
        $row.find('.dk-beli-jumlah-lot').val('');
        $row.find('.dk-beli-jumlah-shares').val('');
        $row.find('.dk-beli-harga-shares').val('');
        getSaldo();
        
    });

    $('.dk-beli-btn-add').click(function(e) {
		e.preventDefault();
		
		var $row = $('.dk-beli-table > tbody').find('tr').first();
		
		$row.find('select.select2').select2('destroy');
		
		var $new_row = $row.clone().appendTo('.dk-beli-table > tbody');
		
		$new_row.find('.select2').val('');
		$new_row.find('input[type=text]').val('');
		
		$('.select2').select2({ allowClear: true });
		$('input.control-number-no').number(true, 0);
        $('input.control-number-digit').number(true, 2);
	});

    $('.dk-beli-table').on('click', '.dk-beli-btn-remove', function(e) {
		e.preventDefault();
		
		var jumlah_row = $('.dk-beli-table > tbody').find('tr').length;
		
		var $row = $(this).closest('tr');
		
		if (jumlah_row == 1) {
			$row.find('.dk-beli-saham').val('').trigger('change');
			$row.find('input[type=text]').val('');
		}
		else {
			$row.remove();
		}
		
		dk_hitung_bruto();
	});

    getSaldo();
});