$().ready(function() {
    //pengguna_form
    $('#id_vendor2').on('change',function(){
        var id_vendor=$('#id_vendor2').val();
        var id_grup=$('#id_grup').val();
        $('#id_vendor').val(id_vendor);
        $("#pengguna_grup").empty();
        $.post (
            site_url+'/options/options/getGrupPengguna'
            , { id_vendor: id_vendor}
            , function(response) {
                response.forEach(element => {
                    if (id_grup == element.id) {
                        $("#pengguna_grup").append("<option selected value='" + element.id + "'>" + element.nama + "</option>");
                    } else {
                        $("#pengguna_grup").append("<option value='" + element.id + "'>" + element.nama + "</option>");
                    }
                    
                });
            }
        );
    });
    $('#id_vendor2').trigger('change');

    $('#id_vendor_fix').on('change',function(){
        var id_vendor=$('#id_vendor_fix').val();
        var id_grup=$('#id_grup').val();
        $('#id_vendor').val(id_vendor);
        $("#pengguna_grup").empty();
        $("#pengguna_grup").append("<option value=''>--Pilih Grup--</option>");
        $.post (
            site_url+'/options/options/getGrupPengguna'
            , { id_vendor: id_vendor}
            , function(response) {
                response.forEach(element => {
                    if (id_grup == element.id) {
                        $("#pengguna_grup").append("<option selected value='" + element.id + "'>" + element.nama + "</option>");
                    } else {
                        $("#pengguna_grup").append("<option value='" + element.id + "'>" + element.nama + "</option>");
                    }
                    
                });
            }
        );
    });
    $('#id_vendor_fix').trigger('change');
})