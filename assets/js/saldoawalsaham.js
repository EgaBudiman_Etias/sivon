function hitung(){
    var lot=$('#shal_jumlah_lot').val();
    var harga=$('#shal_rata_rata_harga').val();
    var shares=parseInt(lot)*100;
    var total=parseInt(lot)*100*parseInt(harga);
    $('#shal_jumlah_shares').val(shares);
    $('#shal_total').val(total);
}
function getData(){
    var akun_id=$('#shal_akun_id').val();
    var sham_id=$('#shal_sham_id').val();
    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;
    console.log(output);
    $.post (
        site_url+'/pengaturan/saldoawalsaham/ajax_saham'
        , { akun_id: akun_id,sham_id:sham_id }
        , function(response) {
        //    console.log(response.shal_tgl_saldo_awal);
           if(response!=null){
               console.log(response);
               $('#shal_tgl_saldo_awal').val(response.shal_tgl_saldo_awal);
               $('#shal_jumlah_lot').val(response.shal_jumlah_lot);
               $('#shal_jumlah_shares').val(response.shal_jumlah_shares);
               $('#shal_rata_rata_harga').val(response.shal_rata_rata_harga);
               $('#shal_total').val(response.shal_total);
               hitung();
           }
           else{
                $('#shal_tgl_saldo_awal').val(output);
                $('#shal_jumlah_lot').val(1);
                $('#shal_jumlah_shares').val(100);
                $('#shal_rata_rata_harga').val(0);
                $('#shal_total').val(0);
                hitung();
           }
        
        }
    );
}

function dk_shal_clear()
{
	var $table = $('.dk-shal-table');
	
	$table.find('.select2').val('').trigger('change');
	$table.find('input[type=text]').val('');
	
	$table.find('tbody tr').not(':first').remove();
}

function dk_hitung_bruto()
{   
    var $sahamx=$('.dk-shal-saham');
	var $jumlah_lotx = $('.dk-shal-jumlah-lot');
    var $jumlah_sharesx = $('.dk-shal-jumlah-shares');
    var $harga_sharesx = $('.dk-shal-rata-rata-harga');
    
    var $brutox = $('.dk-shal-bruto');
    
    var $nettox = $('.dk-shal-netto');
    var netto=0 ;
    var bruto=0;
    var jumlah_shares=0;
    var feerupiah=0;
    var total=0;
    
    $.each ($sahamx, function(i, o) {
        var lot = parseInt($($jumlah_lotx[i]).val());
        var harga = parseInt($($harga_sharesx[i]).val());
        
        
        if (isNaN(lot)) {
			lot = 0;
		}
		
		if (isNaN(harga)) {
			harga = 0;
		}

        jumlah_shares=lot*100;
        $($jumlah_sharesx[i]).val(jumlah_shares);

        bruto=lot*100*harga;
        $($brutox[i]).val(bruto);

        total = total + bruto;

    });
	$('.dk-shal-grand-total').val(total);
}
    function Hapus(){
        window.confirm = function() {};
        var shal_akun_id=$('#shal_akun_id').val();
        $.post (
            site_url+'/pengaturan/saldoawalsaham/delete'
            , { akun_id: shal_akun_id}
            , function(response) {
                location.reload();
            }
        );
    
    }
$().ready(function() {
    $('.dk-shal-table').on('keyup', '.dk-shal-jumlah-lot', dk_hitung_bruto);
    $('.dk-shal-table').on('keyup', '.dk-shal-rata-rata-harga', dk_hitung_bruto);

    $('.dk-shal-table').on('change', '.dk-shal-saham', function() {
        var shal_akun_id=$('#shal_akun_id').val();
        var sham_id=$(this).val();
        var $row = $(this).closest('tr');
        var is_sama = false;
        var $this = $(this);
        
        
        if(sham_id != ''){
            if(shal_akun_id==""){
                alert("Pilih Akun Terlebih Dahulu");
                
                return;  
            };
            $('.dk-shal-saham').not(this).each(function() {
				if (sham_id == $(this).val()) {
					is_sama = true;
				}
			});
            
            
            $row.find('.dk-shal-jumlah-lot').val(1);
            $row.find('.dk-shal-rata-rata-harga').val(1);
            dk_hitung_bruto();
            
        }else{
            $row.find('.dk-shal-jumlah-lot').val(0);
            $row.find('.dk-shal-rata-rata-harga').val(0);
            dk_hitung_bruto();
        }
        
    });

    $('.dk-shal-btn-add').click(function(e) {
		e.preventDefault();
		
		var $row = $('.dk-shal-table > tbody').find('tr').first();
		
		$row.find('select.select2').select2('destroy');
		
		var $new_row = $row.clone().appendTo('.dk-shal-table > tbody');
		
		$new_row.find('.select2').val('');
		$new_row.find('input[type=text]').val('');
		
		$('.select2').select2({ allowClear: true });
		$('input.control-number-no').number(true, 0);
        $('input.control-number-digit').number(true, 2);
	});

    $('#shal_akun_id').on('change',function(){
        dk_shal_clear(); 
        var shal_akun_id=$('#shal_akun_id').val();
        var $table = $('.dk-shal-table');
        $.post (
            site_url+'/master/akun/ajax_akun_awal'
            , { akun_id: shal_akun_id}
            , function(response) {
                
                if(response!=""){
                    $table.find('tbody tr').remove();
                    $('.dk-shal-btn-add').hide();
                    $('#HapusSaham').show();
                }else{
                    $('.dk-shal-btn-add').show();
                    $('#HapusSaham').hide();
                }
                
                $.each(response, function(name, value) {
                    
                    var sham_kode=value.sham_kode;
                    var sham_nama=value.sham_nama;
                    var saham=sham_kode+"-"+sham_nama;
                    var shal_sham_id=value.shal_sham_id;
                    var shal_jumlah_lot=value.shal_jumlah_lot;
                    var shal_jumlah_shares=value.shal_jumlah_shares;
                    var shal_rata_rata_harga=value.shal_rata_rata_harga;
                    var shal_total=value.shal_total;
                    var tgl=value.shal_tgl_saldo_awal;
                    $('#shal_tgl_saldo_awal').val(tgl);
                    markup = '<tr>'+
                        '<td>'+
                            '<select class="form-control select2 dk-shal-saham" name="saham[shal_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">'+
                            
                                '<option value="'+shal_sham_id+'">'+saham+'</option>'+
                                
                            '</select> '+
                        '</td>'+
                        '<td><input type="text" name="saham[shal_jumlah_lot][]" class="control-number-no dk-shal-jumlah-lot" style="width:70px" value="'+shal_jumlah_lot+'" ></td>'+
                        '<td><input type="text" name="saham[shal_jumlah_shares][]" class="control-number-no dk-shal-jumlah-shares" style="width:80px" value="'+shal_jumlah_shares+'" readonly></td>'+
                        '<td><input type="text" name="saham[shal_rata_rata_harga][]" class="control-number-digit dk-shal-rata-rata-harga" style="width:120px" value="'+shal_rata_rata_harga+'"></td>'+
                        '<td><input type="text" name="saham[shal_total][]" class="control-number-digit dk-shal-bruto" style="width:120px" value="'+shal_total+'" readonly></td>'+
								
                        '<td align="center">'+
                            '<button class="btn btn-action btn-danger dk-jual-btn-remove">'+
                                '<i class="ti ti-trash"></i>'+
                            '</button>'+
                        '</td>'+
                    '</tr>'
                    
                    
                    ;
                    // console.log(markup);
                    tableBody = $(".dk-shal-table tbody");
                    tableBody.append(markup);
                    dk_hitung_bruto();
                    
                    $('.select2').select2({ allowClear: true });
                    $('input.control-number-no').number(true, 0);
                    $('input.control-number-digit').number(true, 2);
                });
            }
        );
        
        
    });
    $('.dk-shal-table').on('click', '.dk-jual-btn-remove', function(e) {
		e.preventDefault();
		
		var jumlah_row = $('.dk-shal-table > tbody').find('tr').length;
		
		var $row = $(this).closest('tr');
		
		if (jumlah_row == 1) {
			$row.find('.dk-shal-saham').val('').trigger('change');
			$row.find('input[type=text]').val('');
		}
		else {
			$row.remove();
		}
		
		dk_hitung_bruto();
	});

    
});