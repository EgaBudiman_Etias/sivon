function bool_icon(val)
{
	var ya = '<i class="ti ti-check text-success"></i>';
	var tidak = '<i class="ti ti-close text-danger"></i>';
	return val == 1 ? ya : tidak;
}

function change_provinsi()
{
	var id_provinsi = $('.provinsi-control').val();
	var id_kota = $('.kota-control').data('selected');
	
	$('.kota-control').data('placeholder', 'Memuat data...').select2({ allowClear: true });
	$('.kota-control').children().slice(1).remove();
	
	$.post (
		site_url+'/options/async/kota'
		, { id_provinsi: id_provinsi }
		, function(response) {
			$('.kota-control').data('placeholder', 'Pilih Kab / Kota').select2({ allowClear: true });
			$('.kota-control').append(response).val(id_kota).trigger('change');
			$('.kota-control').data('selected', '');
		}
	);
}

function status(val)
{
	var warna = '#333';
	
	switch (val) {
		case 'OPEN':
			warna = '#007b5f'; break;
		
		case 'PARTIALLY BILLED':
		case 'PARTIALLY DELIVERED':
			warna = 'Peru'; break;
		
		case 'BILLED':
			warna = 'IndianRed'; break;
		
		case 'DELIVERED':
			warna = 'SlateBlue'; break;
		
		case 'CLOSED':
		case 'INVOICED':
			warna = 'CornFlowerBlue'; break;
	}
	
	return '<font class="status" color="'+warna+'">'+val+'</font>';
}


$().ready(function() {
	$('input').attr('autocomplete', 'off');
	$('input.control-number').number(true, 0, ',', '.');
	$('input.control-decimal').number(true, 2, ',', '.');
	$('.select2').select2({ allowClear: true });
	$('.select2-no-clear').select2({ allowClear: false });
	$('.select2-tags').select2({ tags: true });
	
	$('.custom-file-input').on('change', function() {
		var file_name = $(this).val().split("\\").pop();
		$(this).siblings('.custom-file-label').addClass('selected').html(file_name);
	});
	
	$('#datatable').on('click', '.btn-delete', function(e) {
		return confirm('Apakah Anda yakin menghapus data ini?');
	});
	
	if ($('select').hasClass('provinsi-control')) {
		change_provinsi();
		$('.provinsi-control').change(change_provinsi);
	}
	
	$('.dk-form').submit(function(e) {
		
		if ($(this).hasClass('dk-form-confirm')) {
			var is_konfirmasi = confirm('Apakah Anda yakin menyimpan data ini?');
			
			if (is_konfirmasi) {
				$(this).find('[type=submit]').text('Menyimpan data...').attr('disabled', true);
				return true;
			}
			
			return false;
		}
		
		$(this).find('[type=submit]').text('Menyimpan data...').attr('disabled', true);

	});
});

function rupiahFormat(num){
    var ribuan = "";
    var reverse = num.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    rupiah  = "Rp"+ribuan;
    return rupiah;
}