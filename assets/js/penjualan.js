function dk_jual_clear()
{
	var $table = $('.dk-jual-table');
	
	$table.find('.select2').val('').trigger('change');
	$table.find('input[type=text]').val('');
	
	$table.find('tbody tr').not(':first').remove();
}
function get_fee_penjualan(pnjl_akun_id){
    var $jumlah_lot = $('.dk-jual-jumlah-lot');
    
    $.post (
        site_url+'/master/akun/ajax_akun'
        , { pnjl_akun_id: pnjl_akun_id }
        , function(response) {
           return response;
        }
    );
}   

function dk_hitung_bruto()
{   
    var $sahamx=$('.dk-jual-saham');
	var $jumlah_lotx = $('.dk-jual-jumlah-lot');
    var $jumlah_sharesx = $('.dk-jual-jumlah-shares');
    var $harga_sharesx = $('.dk-jual-harga-shares');
    var $feex = $('.dk-jual-fee-penjualan');
    var $brutox = $('.dk-jual-bruto');
    var $feerupiahx = $('.dk-jual-fee-penjualan-rp');
    var $nettox = $('.dk-jual-netto');
    var pajakpersen=$('#pajak').val();
    var $pajakx = $('.dk-jual-pajak');
    var netto=0 ;
    var bruto=0;
    var jumlah_shares=0;
    var feerupiah=0;
    var total=0;
    var pajak=0;
    
    $.each ($sahamx, function(i, o) {
        var lot = parseInt($($jumlah_lotx[i]).val());
        var harga = parseInt($($harga_sharesx[i]).val());
        var feepersen = parseFloat($($feex[i]).val());
        
        if (isNaN(lot)) {
			lot = 0;
		}
		
		if (isNaN(harga)) {
			harga = 0;
		}

        if (isNaN(feepersen)) {
			feepersen = 0;
		}
        jumlah_shares=lot*100;
        $($jumlah_sharesx[i]).val(jumlah_shares);

        bruto=lot*100*harga;
        $($brutox[i]).val(bruto);

        feerupiah=bruto*feepersen/100;
        $($feerupiahx[i]).val(feerupiah);

        pajak=bruto*pajakpersen/100;
        $($pajakx[i]).val(pajak);

        netto=bruto-feerupiah-pajak;
        $($nettox[i]).val(netto);

        total = total + netto;

    });
	$('.dk-jual-total-harga').val(total);
	
}
function getSaldo(){
    var pnjl_akun_id=$('#pnjl_akun_id').val();
    console.log(pnjl_akun_id);
    if(pnjl_akun_id!='0'){
        $.post (
            site_url+'/master/akun/ajax_akun_nominal'
            , { akun_id: pnjl_akun_id }
            , function(msg) {
                $('#saldo_rdn').val(msg.nominal);
            }
        );
    }else{
        $('#saldo_rdn').val('0');
    }
    
}
function cekLot(){
    var $sahamx=$('.dk-jual-saham');
    var $jumlah_lotx = $('.dk-jual-jumlah-lot');
    var $jumlah_lot_skrx = $('.dk-jual-jumlah-lot-saat-ini');
    $.each ($sahamx, function(i, o) {
        var lot = parseInt($($jumlah_lotx[i]).val());
        var lot2 = parseInt($($jumlah_lot_skrx[i]).val());
        if (isNaN(lot)) {
			lot = 0;
		}
        if (isNaN(lot2)) {
			lot2 = 0;
		}

        if(lot>lot2){
            lot=lot2;
            $($jumlah_lotx[i]).val(lot);
            // $('.dk-jual-jumlah-lot').val(lot);
        }

    });
}

$().ready(function() {
    $('.dk-jual-table').on('keyup', '.dk-jual-jumlah-lot', dk_hitung_bruto);
    $('.dk-jual-table').on('keyup', '.dk-jual-harga-shares', dk_hitung_bruto);
    $('.dk-jual-table').on('keyup', '.dk-jual-jumlah-lot', cekLot);
    $('.dk-jual-table').on('change', '.dk-jual-saham', function() {
        var pnjl_akun_id=$('#pnjl_akun_id').val();
        var sham_id=$(this).val();
        var $row = $(this).closest('tr');
        var is_sama = false;
        var $this = $(this);
        var $jumlah_lot = $('.dk-jual-jumlah-lot');


        if(sham_id != ''){
            if(pnjl_akun_id==""){
                alert("Pilih Akun Terlebih Dahulu");
                return;  
            };
            // $('.dk-jual-saham').not(this).each(function() {
			// 	if (sham_id == $(this).val()) {
			// 		is_sama = true;
			// 	}
			// });
            // if (is_sama) {
			// 	alert('Saham sudah dipilih sebelumnya!');
				
			// 	$this.val('').trigger('change');
			// 	$row.find('input[type=text]').val('');
			// 	return;
			// };
        
            $.post (
                site_url+'/master/akun/ajax_akun'
                , { pmbl_akun_id: pnjl_akun_id ,sham_id:sham_id}
                , function(response) {
                    $row.find('.dk-jual-fee-penjualan').val(response.seku_broker_fee_penjualan);
                    $row.find('.dk-jual-jumlah-lot-saat-ini').val(response.saldosaham);
                    $row.find('.dk-jual-jumlah-lot').val(1);
                    $row.find('.dk-jual-harga-shares').val(1);
                    dk_hitung_bruto();
                   
                }
            );
        };
        
    });
    getSaldo();
    $('#pnjl_akun_id').on('change',function(){
        dk_jual_clear();
        var pnjl_akun_id=$('#pnjl_akun_id').val();
        var $table = $('.dk-jual-table');
        $('#pnjl_akun_id2').val(pnjl_akun_id);
        var $row = $('.dk-jual-table').closest('tr');
        $row.find('.dk-jual-fee-penjualan').val('');
        $row.find('.dk-jual-jumlah-lot').val('');
        $row.find('.dk-jual-harga-shares').val('');
        
        getSaldo();
        $.post (
            site_url+'/master/akun/ajax_akun_saham'
            , { akun_id: pnjl_akun_id}
            , function(response) {
                $table.find('tbody tr').remove();
                // parse = $.parseJSON(response);
                
                $.each(response, function(name, value) {
                    console.log(response);
                    var sham_kode=value.sham_kode;
                    var sham_nama=value.sham_nama;
                    var saham=sham_kode+"-"+sham_nama;
                    var kshm_sham_id=value.kshm_sham_id;
                    var jumlah_lot=value.jumlah;
                    var seku_broker_fee_penjualan=value.seku_broker_fee_penjualan;
                    var markup="";
                    if(jumlah_lot!=0){
                        markup = '<tr>'+
                            '<td>'+
                                '<select class="form-control select2 dk-jual-saham" name="saham[dpjl_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">'+
                                    '<option value="'+kshm_sham_id+'">'+saham+'</option>'+
                                    
                                '</select> '+
                                
                            '</td>'+
                            '<td><input type="text" name="saham[dpjl_jumlah_lot_saat_ini][]" class="control-number-no dk-jual-jumlah-lot-saat-ini" value="'+jumlah_lot+'"  style="width:100%" readonly ></td>'+
                            '<td><input type="text" name="saham[dpjl_jumlah_lot][]" class="control-number-no dk-jual-jumlah-lot" style="width:100%" value="0" ></td>'+
                            '<td><input type="text" name="saham[dpjl_jumlah_shares][]" class="control-number-no dk-jual-jumlah-shares" style="width:100%" value="100" readonly></td>'+
                            '<td><input type="text" name="saham[dpjl_harga_shares][]" class="control-number-digit dk-jual-harga-shares" style="width:100%" value="0"></td>'+
                            '<td><input type="text" name="saham[dpjl_bruto][]" class="control-number-digit dk-jual-bruto" style="width:100%" value="0" readonly></td>'+
                            '<td>'+
                                '<input type="hidden" name="saham[dpjl_fee_penjualan_persen][]"  class="control-number-no dk-jual-fee-penjualan" style="width:100%" value="'+seku_broker_fee_penjualan+'">'+
                                '<input type="text" name="saham[dpjl_fee_penjualan][]"  class="control-number-digit dk-jual-fee-penjualan-rp" readonly style="width:100%" value="0">'+
                            '</td>'+
                            '<td><input type="text" name="saham[dpjl_pajak_penjualan][]" class="control-number-digit dk-jual-pajak" style="width:100%" value="0" readonly></td>'+
                            '<td><input type="text" class="control-number-digit dk-jual-netto" name="saham[dpjl_netto][]" style="width:100%" value="0" readonly></td>'+
                            
                            '<td align="center">'+
                                '<button class="btn btn-action btn-danger dk-jual-btn-remove">'+
                                    '<i class="ti ti-trash"></i>'+
                                '</button>'+
                            '</td>'+
                        '</tr>';
                    }
                    // console.log(markup);
                    tableBody = $(".dk-jual-table tbody");
                    tableBody.append(markup);
                    dk_hitung_bruto();
                    // var $row = $('.dk-jual-table > tbody').find('tr').first();
		
                    // $row.find('select.select2').select2('destroy');
                    
                    // var $new_row = $row.clone().appendTo('.dk-jual-table > tbody');
                    
                    // $new_row.find('.select2').val('');
                    // $new_row.find('input[type=text]').val('');
                    
                    $('.select2').select2({ allowClear: true });
                    $('input.control-number-no').number(true, 0);
                    $('input.control-number-digit').number(true, 2);
                    $('input').attr('autocomplete', 'off');
                });
            }
        );
    });

    $('.dk-jual-btn-add').click(function(e) {
		e.preventDefault();
		
		var $row = $('.dk-jual-table > tbody').find('tr').first();
		
		$row.find('select.select2').select2('destroy');
		
		var $new_row = $row.clone().appendTo('.dk-jual-table > tbody');
		
		$new_row.find('.select2').val('');
		$new_row.find('input[type=text]').val('');
		
		$('.select2').select2({ allowClear: true });
		$('input.control-number-no').number(true, 0);
        $('input.control-number-digit').number(true, 2);
	});

    $('.dk-jual-table').on('click', '.dk-jual-btn-remove', function(e) {
		e.preventDefault();
		
		var jumlah_row = $('.dk-jual-table > tbody').find('tr').length;
		
		var $row = $(this).closest('tr');
		
		if (jumlah_row == 1) {
			$row.find('.dk-jual-saham').val('').trigger('change');
			$row.find('input[type=text]').val('');
		}
		else {
			$row.remove();
		}
		
		dk_hitung_bruto();
	});
});