/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : sivon

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 25/01/2022 20:10:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for det_invoice
-- ----------------------------
DROP TABLE IF EXISTS `det_invoice`;
CREATE TABLE `det_invoice`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_invoice` int NULL DEFAULT NULL,
  `id_produk` int NULL DEFAULT NULL,
  `jumlah` int NULL DEFAULT NULL,
  `harga` double NULL DEFAULT NULL,
  `diskon` double NULL DEFAULT NULL,
  `pajak` double NULL DEFAULT NULL,
  `bruto` double NULL DEFAULT NULL,
  `netto` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of det_invoice
-- ----------------------------

-- ----------------------------
-- Table structure for invoice
-- ----------------------------
DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `id_pelanggan` int NULL DEFAULT NULL,
  `no_invoice` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_invoice` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_terbit` date NULL DEFAULT NULL,
  `tgl_jatuh_tempo` date NULL DEFAULT NULL,
  `total` double NULL DEFAULT NULL,
  `diskon` double NULL DEFAULT NULL,
  `netto` double NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoice
-- ----------------------------

-- ----------------------------
-- Table structure for konfigurasi_gambar
-- ----------------------------
DROP TABLE IF EXISTS `konfigurasi_gambar`;
CREATE TABLE `konfigurasi_gambar`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `header_dokumen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `footed_dokumen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ttd_validator` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `header_kwitansi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `logo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_konfigurasi_gambar`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_konfigurasi_gambar` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of konfigurasi_gambar
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `id_induk` int NULL DEFAULT NULL,
  `kode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nomor` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ikon` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `teks` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `uri` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_menu`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_menu` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 1, NULL, 'dasbor', '010.000', 'dashboard', 'Dashboard', '/dasboard', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (2, 1, NULL, 'transaksi', '020.000', 'face-smile', 'Transaksi', '#', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (3, 1, NULL, 'laporan', '030.000', 'money', 'Laporan', '#', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (4, 1, NULL, 'master', '040.000', 'harddrive', 'Master', '#', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (5, 1, NULL, 'pengaturan', '050.000', 'settings', 'Pengaturan', '#', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (6, 1, 4, 'vendor', '040.100', NULL, 'Vendor', '/master/vendor', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (7, 1, 5, 'pengguna', '050.100', NULL, 'Pengguna', '/pengaturan/pengguna', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (8, 1, 5, 'grup', '050.200', NULL, 'Grup Pengguna', '/pengaturan/penggunagrup', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (9, 1, 5, 'menu', '050.300', NULL, 'Menu Grup', '/pengaturan/penggunagrupmenu', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (10, 1, 4, 'pelanggan', '040.200', NULL, 'Pelanggan', '/master/pelanggan', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (11, 1, 4, 'produk', '040.300', NULL, 'Produk', '/master/produk', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `menu` VALUES (12, 1, 2, 'invoice', '020.100', NULL, 'Invoice', '/transaksi/invoice', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');

-- ----------------------------
-- Table structure for nomor
-- ----------------------------
DROP TABLE IF EXISTS `nomor`;
CREATE TABLE `nomor`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `kode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `format_nomor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `digit_serial` tinyint NOT NULL,
  `reset_serial` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tahun_sekarang` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bulan_sekarang` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `serial_berikutnya` int NOT NULL,
  `is_deleted` enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1',
  `created_time` datetime NOT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_time` datetime NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_nomor`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_nomor` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of nomor
-- ----------------------------
INSERT INTO `nomor` VALUES (1, 2, 'invoice', 'Invoice', 'INV@y2@m@serial', 4, 'bulanan', '2022', '1', 0, '', '2021-01-16 12:00:00', NULL, '2021-12-04 19:16:58', NULL);
INSERT INTO `nomor` VALUES (2, 2, 'registrasi', 'Registrasi', '2125@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-01-17 20:00:00', NULL, '2021-12-05 12:19:27', NULL);
INSERT INTO `nomor` VALUES (3, 2, 'perubahan', 'Perubahan Layanan', 'CH@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-01-21 05:00:00', NULL, '2021-11-24 16:54:06', NULL);
INSERT INTO `nomor` VALUES (4, 2, 'spk_pasang', 'SPK Pasang Baru', 'PSG@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-01-28 10:00:00', NULL, '2021-12-05 12:19:27', NULL);
INSERT INTO `nomor` VALUES (5, 2, 'berhenti', 'Berhenti Berlangganan', 'OFF@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-02-08 16:00:00', NULL, '2021-11-22 15:02:22', NULL);
INSERT INTO `nomor` VALUES (6, 2, 'spk_mainline', 'SPK Mainline', 'ML@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-03-08 14:30:00', NULL, '2021-10-18 17:12:17', NULL);
INSERT INTO `nomor` VALUES (7, 2, 'penanganan', 'Penanganan Pelanggan', 'TIC@y2@m@serial', 2, 'bulanan', '2022', '1', 0, '', '2021-04-10 09:00:00', NULL, '2021-12-04 19:11:46', NULL);
INSERT INTO `nomor` VALUES (8, 2, 'inventory', 'Inventory', 'STK@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-01-21 05:00:00', NULL, '2021-12-03 09:45:38', NULL);
INSERT INTO `nomor` VALUES (9, 2, 'pelanggan', 'pelanggan', '@serial', 5, 'terus', '2022', '1', 9, '', '2021-01-21 05:00:00', NULL, '2021-12-04 16:50:37', NULL);
INSERT INTO `nomor` VALUES (10, 2, 'produk', 'Produk', 'P@serial', 6, 'terus', '2022', '1', 2, '', '2021-01-28 10:00:00', NULL, '2021-11-24 16:56:49', NULL);
INSERT INTO `nomor` VALUES (11, 2, 'spk_berhenti', 'SPK Berhenti', 'S-OFF@y2@m@serial', 3, 'bulanan', '2022', '1', 0, '', '2021-01-28 10:00:00', NULL, '2021-12-03 09:44:24', NULL);

-- ----------------------------
-- Table structure for pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `kode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_registrasi` date NULL DEFAULT NULL,
  `nama` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_owner` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_pelanggan`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_pelanggan` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pelanggan
-- ----------------------------
INSERT INTO `pelanggan` VALUES (1, 2, '00001', '2022-01-01', 'as', 'as', 'sad', 'as', 3, '2022-01-23 14:20:38', 3, '2022-01-23 15:57:42', '1');
INSERT INTO `pelanggan` VALUES (2, 2, '00002', '2022-01-01', 'sadx', 'sadx', 'asdx', 'asdx', 3, '2022-01-25 19:03:26', 3, '2022-01-25 19:03:45', '1');
INSERT INTO `pelanggan` VALUES (3, NULL, '00006', '2022-01-01', 'Jono', 'Jono', 'Y', 'a@gmail.com', NULL, NULL, NULL, NULL, '1');
INSERT INTO `pelanggan` VALUES (4, NULL, '00007', '2022-01-02', 'Jono2', 'Jono2', 'Y2', 'a@gmail.com2', NULL, NULL, NULL, NULL, '1');
INSERT INTO `pelanggan` VALUES (5, NULL, '00008', '2022-01-03', 'Jono3', 'Jono3', 'Y3', 'a@gmail.com3', NULL, NULL, NULL, NULL, '1');

-- ----------------------------
-- Table structure for pengguna
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `id_pengguna_grup` int NULL DEFAULT NULL,
  `nama_pengguna` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_lengkap` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cookie` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `surel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_wa` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_pengguna`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_pengguna` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengguna
-- ----------------------------
INSERT INTO `pengguna` VALUES (1, 1, 1, 'dinata', 'Arga Dinata', '2986032a8c843640542c6dad2e30b8cf', 'f05fcba70f9684806f02629d0354ecdc', NULL, NULL, 1, '2022-01-22 09:21:00', 1, '2022-01-22 10:46:53', '1');
INSERT INTO `pengguna` VALUES (2, 1, 2, 'budiman', 'Ega Budiman', 'ca8d0008e4c6f07424c230ec4fd35263', NULL, NULL, NULL, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna` VALUES (3, 2, 3, 'ary', 'Aryanti', '2986032a8c843640542c6dad2e30b8cf', '88a3cc72fe5ef98fba66b0edc4bd6f08', NULL, NULL, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna` VALUES (4, 2, 4, 'aqila', NULL, 'ef5d3db46696b9dcd4095aee8ccfe589', NULL, 'aqila@gmail.com', '1111', 1, NULL, 1, '2022-01-22 13:28:51', '1');

-- ----------------------------
-- Table structure for pengguna_grup
-- ----------------------------
DROP TABLE IF EXISTS `pengguna_grup`;
CREATE TABLE `pengguna_grup`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `kode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `urutan` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_pengguna_grup`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_pengguna_grup` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengguna_grup
-- ----------------------------
INSERT INTO `pengguna_grup` VALUES (1, 1, 'OP', 'Owner Pollux', '1', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup` VALUES (2, 1, 'AP', 'Admin Pollux', '2', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup` VALUES (3, 2, 'OV', 'Owner Vendor', '1', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup` VALUES (4, 2, 'AV', 'Admin Vendor', '2', 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');

-- ----------------------------
-- Table structure for pengguna_grup_menu
-- ----------------------------
DROP TABLE IF EXISTS `pengguna_grup_menu`;
CREATE TABLE `pengguna_grup_menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `id_pengguna_grup` int NULL DEFAULT NULL,
  `id_menu` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_pengguna_grup_menu`(`id_vendor` ASC) USING BTREE,
  INDEX `FK_pengguna_grup_menu_1`(`id_pengguna_grup` ASC) USING BTREE,
  CONSTRAINT `FK_pengguna_grup_menu` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_pengguna_grup_menu_1` FOREIGN KEY (`id_pengguna_grup`) REFERENCES `pengguna_grup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengguna_grup_menu
-- ----------------------------
INSERT INTO `pengguna_grup_menu` VALUES (1, 1, 1, 1, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (2, 1, 1, 2, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (3, 1, 1, 3, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (4, 1, 1, 4, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (5, 1, 1, 5, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (6, 1, 1, 6, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (7, 1, 1, 7, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (8, 1, 1, 8, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (9, 1, 1, 9, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (10, 1, 2, 1, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (11, 1, 2, 2, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (12, 1, 2, 3, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (13, 1, 2, 4, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (14, 1, 2, 5, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (15, 1, 2, 6, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (16, 1, 2, 7, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (17, 1, 2, 8, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (18, 1, 2, 9, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (19, 2, 3, 1, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (20, 2, 3, 2, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (21, 2, 3, 3, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (22, 2, 3, 4, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (23, 2, 3, 5, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (27, 1, 1, 10, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (28, 1, 1, 11, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (29, 1, 2, 10, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (30, 1, 2, 11, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (31, 2, 3, 10, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (32, 2, 3, 11, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (33, 1, 1, 12, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (34, 1, 2, 12, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (35, 2, 3, 12, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');
INSERT INTO `pengguna_grup_menu` VALUES (36, 2, 4, 12, 1, '2022-01-22 09:21:00', 1, '2022-01-22 09:21:00', '1');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `kode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_produk` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `harga` double NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_produk`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_produk` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES (1, 2, 'P000000', 'A09', 'Tes', 10000000, 'asd', 3, '2022-01-25 19:44:41', 3, '2022-01-25 19:44:41', '1');
INSERT INTO `produk` VALUES (2, NULL, NULL, 'P0001', 'Jaringan', 1000000, 'tes aja dulu', 3, '2022-01-25 20:05:49', 3, '2022-01-25 20:05:49', '1');

-- ----------------------------
-- Table structure for ref_pilihan
-- ----------------------------
DROP TABLE IF EXISTS `ref_pilihan`;
CREATE TABLE `ref_pilihan`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `kategori` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `urutan` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_ref_pilihan`(`id_vendor` ASC) USING BTREE,
  CONSTRAINT `FK_ref_pilihan` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ref_pilihan
-- ----------------------------

-- ----------------------------
-- Table structure for vendor
-- ----------------------------
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `kode` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_vendor` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_owner` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tahun` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_time` datetime NULL DEFAULT NULL,
  `is_deleted` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vendor
-- ----------------------------
INSERT INTO `vendor` VALUES (1, 'V0001', 'Pollux', 'Arga Dinata', '2022', 1, '2022-01-22 09:21:00', 3, '2022-01-23 15:56:49', '1');
INSERT INTO `vendor` VALUES (2, 'V0002', 'Asnet Home', 'Mr X', '2022', 1, '2022-01-22 09:21:00', NULL, '2022-01-22 09:21:00', '1');

SET FOREIGN_KEY_CHECKS = 1;
