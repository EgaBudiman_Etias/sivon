/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.5.5-10.4.14-MariaDB : Database - sivon
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`sivon` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sivon`;

/*Table structure for table `det_invoice` */

DROP TABLE IF EXISTS `det_invoice`;

CREATE TABLE `det_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_invoice` int(11) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `pajak` double DEFAULT NULL,
  `bruto` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `det_invoice` */

/*Table structure for table `invoice` */

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `no_invoice` varchar(30) DEFAULT NULL,
  `jenis_invoice` varchar(30) DEFAULT NULL,
  `tgl_terbit` date DEFAULT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `total` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `invoice` */

/*Table structure for table `konfigurasi_gambar` */

DROP TABLE IF EXISTS `konfigurasi_gambar`;

CREATE TABLE `konfigurasi_gambar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `header_dokumen` text DEFAULT NULL,
  `footed_dokumen` text DEFAULT NULL,
  `ttd_validator` text DEFAULT NULL,
  `header_kwitansi` text DEFAULT NULL,
  `logo` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_konfigurasi_gambar` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_konfigurasi_gambar` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `konfigurasi_gambar` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `id_induk` int(11) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `nomor` varchar(15) DEFAULT NULL,
  `ikon` varchar(30) DEFAULT NULL,
  `teks` varchar(100) DEFAULT NULL,
  `uri` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_menu` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_menu` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `menu` */

insert  into `menu`(`id`,`id_vendor`,`id_induk`,`kode`,`nomor`,`ikon`,`teks`,`uri`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,1,NULL,'dasbor','010.000','dashboard','Dashboard','/dasboard',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(2,1,NULL,'transaksi','020.000','face-smile','Transaksi','#',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(3,1,NULL,'laporan','030.000','money','Laporan','#',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(4,1,NULL,'master','040.000','harddrive','Master','#',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(5,1,NULL,'pengaturan','050.000','settings','Pengaturan','#',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(6,1,4,'vendor','040.100',NULL,'Vendor','/master/vendor',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(7,1,5,'pengguna','050.100',NULL,'Pengguna','/pengaturan/pengguna',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(8,1,5,'grup','050.200',NULL,'Grup Pengguna','/pengaturan/penggunagrup',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(9,1,5,'menu','050.300',NULL,'Menu Grup','/pengaturan/penggunagrupmenu',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(10,1,4,'pelanggan','040.200',NULL,'Pelanggan','/master/pelanggan',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(11,1,4,'produk','040.300',NULL,'Produk','/master/produk',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(12,1,2,'invoice','020.100',NULL,'Invoice','/transaksi/invoice',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1');

/*Table structure for table `nomor` */

DROP TABLE IF EXISTS `nomor`;

CREATE TABLE `nomor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `format_nomor` varchar(50) NOT NULL,
  `digit_serial` tinyint(4) NOT NULL,
  `reset_serial` varchar(50) NOT NULL,
  `tahun_sekarang` char(4) NOT NULL,
  `bulan_sekarang` varchar(2) NOT NULL,
  `serial_berikutnya` int(11) NOT NULL,
  `is_deleted` enum('1','0') NOT NULL DEFAULT '1',
  `created_time` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_nomor` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_nomor` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `nomor` */

insert  into `nomor`(`id`,`id_vendor`,`kode`,`nama`,`format_nomor`,`digit_serial`,`reset_serial`,`tahun_sekarang`,`bulan_sekarang`,`serial_berikutnya`,`is_deleted`,`created_time`,`created_by`,`updated_time`,`updated_by`) values (1,2,'invoice','Invoice','INV@y2@m@serial',4,'bulanan','2022','1',0,'','2021-01-16 12:00:00',NULL,'2021-12-04 19:16:58',NULL),(2,2,'registrasi','Registrasi','2125@y2@m@serial',3,'bulanan','2022','1',0,'','2021-01-17 20:00:00',NULL,'2021-12-05 12:19:27',NULL),(3,2,'perubahan','Perubahan Layanan','CH@y2@m@serial',3,'bulanan','2022','1',0,'','2021-01-21 05:00:00',NULL,'2021-11-24 16:54:06',NULL),(4,2,'spk_pasang','SPK Pasang Baru','PSG@y2@m@serial',3,'bulanan','2022','1',0,'','2021-01-28 10:00:00',NULL,'2021-12-05 12:19:27',NULL),(5,2,'berhenti','Berhenti Berlangganan','OFF@y2@m@serial',3,'bulanan','2022','1',0,'','2021-02-08 16:00:00',NULL,'2021-11-22 15:02:22',NULL),(6,2,'spk_mainline','SPK Mainline','ML@y2@m@serial',3,'bulanan','2022','1',0,'','2021-03-08 14:30:00',NULL,'2021-10-18 17:12:17',NULL),(7,2,'penanganan','Penanganan Pelanggan','TIC@y2@m@serial',2,'bulanan','2022','1',0,'','2021-04-10 09:00:00',NULL,'2021-12-04 19:11:46',NULL),(8,2,'inventory','Inventory','STK@y2@m@serial',3,'bulanan','2022','1',0,'','2021-01-21 05:00:00',NULL,'2021-12-03 09:45:38',NULL),(9,2,'pelanggan','pelanggan','@serial',5,'terus','2022','1',10,'','2021-01-21 05:00:00',NULL,'2021-12-04 16:50:37',NULL),(10,2,'produk','Produk','P@serial',6,'terus','2022','1',2,'','2021-01-28 10:00:00',NULL,'2021-11-24 16:56:49',NULL),(11,2,'spk_berhenti','SPK Berhenti','S-OFF@y2@m@serial',3,'bulanan','2022','1',0,'','2021-01-28 10:00:00',NULL,'2021-12-03 09:44:24',NULL);

/*Table structure for table `pelanggan` */

DROP TABLE IF EXISTS `pelanggan`;

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `kode` varchar(10) DEFAULT NULL,
  `tgl_registrasi` date DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `nama_kontak` varchar(30) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `no_wa` varchar(12) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_pelanggan` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_pelanggan` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `pelanggan` */

insert  into `pelanggan`(`id`,`id_vendor`,`kode`,`tgl_registrasi`,`nama`,`nama_kontak`,`alamat`,`email`,`no_hp`,`no_wa`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,2,'00001','2022-01-01','as','as','sad','as',NULL,NULL,3,'2022-01-23 14:20:38',3,'2022-01-23 15:57:42','1'),(2,2,'00002','2022-01-01','sadx','sadx','asdx','asdx',NULL,NULL,3,'2022-01-25 19:03:26',3,'2022-01-25 19:03:45','1'),(3,NULL,'00006','2022-01-01','Jono','Jono','Y','a@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'1'),(4,NULL,'00007','2022-01-02','Jono2','Jono2','Y2','a@gmail.com2',NULL,NULL,NULL,NULL,NULL,NULL,'1'),(5,NULL,'00008','2022-01-03','Jono3','Jono3','Y3','a@gmail.com3',NULL,NULL,NULL,NULL,NULL,NULL,'1'),(6,2,'00009','2022-01-02','Budiman Store','Ega','askjkasj','Ega@gmail',NULL,NULL,3,'2022-01-26 14:28:19',3,'2022-01-26 14:28:19','1'),(7,2,'asssaa','2022-01-05','ega','ega','asdasd','aaaa','2222231','1212',3,'2022-01-27 09:17:46',3,'2022-01-27 09:17:46','1'),(8,2,'asd','2022-01-04','asd','asd','sdf','sdf','sdf','dds',3,'2022-01-27 09:18:45',3,'2022-01-27 09:18:45','1'),(9,2,'asd','2022-01-05','sad','sd','asd','asd','asd','asd',3,'2022-01-27 09:20:34',3,'2022-01-27 09:20:34','1');

/*Table structure for table `pembayaran` */

DROP TABLE IF EXISTS `pembayaran`;

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_invoice` int(11) DEFAULT NULL,
  `no_pembayaran` varchar(20) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `metode` varchar(10) DEFAULT NULL,
  `bank_transfer` varchar(20) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `created_by` int(4) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(4) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_pembayaran` (`id_invoice`),
  CONSTRAINT `FK_pembayaran` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pembayaran` */

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `id_pengguna_grup` int(11) DEFAULT NULL,
  `nama_pengguna` varchar(30) DEFAULT NULL,
  `nama_lengkap` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `cookie` varchar(50) DEFAULT NULL,
  `surel` varchar(50) DEFAULT NULL,
  `no_wa` varchar(13) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_pengguna` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_pengguna` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna` */

insert  into `pengguna`(`id`,`id_vendor`,`id_pengguna_grup`,`nama_pengguna`,`nama_lengkap`,`password`,`cookie`,`surel`,`no_wa`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,1,1,'dinata','Arga Dinata','2986032a8c843640542c6dad2e30b8cf',NULL,NULL,NULL,1,'2022-01-22 09:21:00',1,'2022-01-22 10:46:53','1'),(2,1,2,'budiman','Ega Budiman','ca8d0008e4c6f07424c230ec4fd35263',NULL,NULL,NULL,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(3,2,3,'ary','Aryanti','2986032a8c843640542c6dad2e30b8cf','9b0ca2c58c282f391856fb917dedf3b1',NULL,NULL,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(4,2,4,'aqila',NULL,'ef5d3db46696b9dcd4095aee8ccfe589',NULL,'aqila@gmail.com','1111',1,NULL,1,'2022-01-22 13:28:51','1');

/*Table structure for table `pengguna_grup` */

DROP TABLE IF EXISTS `pengguna_grup`;

CREATE TABLE `pengguna_grup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `urutan` varchar(2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_pengguna_grup` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_pengguna_grup` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna_grup` */

insert  into `pengguna_grup`(`id`,`id_vendor`,`kode`,`nama`,`urutan`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,1,'OP','Owner Pollux','1',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(2,1,'AP','Admin Pollux','2',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(3,2,'OV','Owner Vendor','1',1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1');

/*Table structure for table `pengguna_grup_menu` */

DROP TABLE IF EXISTS `pengguna_grup_menu`;

CREATE TABLE `pengguna_grup_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `id_pengguna_grup` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_pengguna_grup_menu` (`id_vendor`) USING BTREE,
  KEY `FK_pengguna_grup_menu_1` (`id_pengguna_grup`) USING BTREE,
  CONSTRAINT `FK_pengguna_grup_menu` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pengguna_grup_menu_1` FOREIGN KEY (`id_pengguna_grup`) REFERENCES `pengguna_grup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `pengguna_grup_menu` */

insert  into `pengguna_grup_menu`(`id`,`id_vendor`,`id_pengguna_grup`,`id_menu`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,1,1,1,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(2,1,1,2,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(3,1,1,3,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(4,1,1,4,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(5,1,1,5,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(6,1,1,6,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(7,1,1,7,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(8,1,1,8,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(9,1,1,9,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(10,1,2,1,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(11,1,2,2,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(12,1,2,3,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(13,1,2,4,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(14,1,2,5,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(15,1,2,6,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(16,1,2,7,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(17,1,2,8,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(18,1,2,9,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(19,2,3,1,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(20,2,3,2,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(21,2,3,3,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(22,2,3,4,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(23,2,3,5,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(27,1,1,10,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(28,1,1,11,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(29,1,2,10,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(30,1,2,11,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(31,2,3,10,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(32,2,3,11,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(33,1,1,12,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(34,1,2,12,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(35,2,3,12,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1'),(37,2,3,7,1,'2022-01-22 09:21:00',1,'2022-01-22 09:21:00','1');

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `kode` varchar(10) DEFAULT NULL,
  `kode_produk` varchar(20) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_produk` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_produk` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `produk` */

insert  into `produk`(`id`,`id_vendor`,`kode`,`kode_produk`,`nama`,`harga`,`deskripsi`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,2,'P000000','A09','Tes',10000000,'asd',3,'2022-01-25 19:44:41',3,'2022-01-25 19:44:41','1'),(2,NULL,NULL,'P0001','Jaringan',1000000,'tes aja dulu',3,'2022-01-25 20:05:49',3,'2022-01-25 20:05:49','1');

/*Table structure for table `ref_pilihan` */

DROP TABLE IF EXISTS `ref_pilihan`;

CREATE TABLE `ref_pilihan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) DEFAULT NULL,
  `kategori` varchar(20) DEFAULT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `urutan` varchar(2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_ref_pilihan` (`id_vendor`) USING BTREE,
  CONSTRAINT `FK_ref_pilihan` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `ref_pilihan` */

/*Table structure for table `vendor` */

DROP TABLE IF EXISTS `vendor`;

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(5) DEFAULT NULL,
  `nama_vendor` varchar(30) DEFAULT NULL,
  `nama_owner` varchar(40) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `vendor` */

insert  into `vendor`(`id`,`kode`,`nama_vendor`,`nama_owner`,`tahun`,`created_by`,`created_time`,`updated_by`,`updated_time`,`is_deleted`) values (1,'V0001','Pollux','Arga Dinata','2022',1,'2022-01-22 09:21:00',3,'2022-01-23 15:56:49','1'),(2,'V0002','Asnet Home','Mr X','2022',1,'2022-01-22 09:21:00',NULL,'2022-01-22 09:21:00','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;