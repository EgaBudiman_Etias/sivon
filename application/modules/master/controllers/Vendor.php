<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'vendor_index',
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from vendor
            
            where
				is_deleted = '1'
				and (
					kode like ?
                    or nama_vendor like ?
                    or nama_owner like ?
					
				)
		";
		
		$data_sql = "
			select
				*
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, kode {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'kode' => $row->kode,
				'nama_vendor' => $row->nama_vendor,
				'nama_owner' => $row->nama_owner,
				'tahun' => $row->tahun,
                'id'=>$row->id,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'vendor_form',
			'url_aksi' => site_url("/master/vendor/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->select('*')
			->from('vendor')
			->where(array('is_deleted'=>'1','id'=>$id))
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'nama_owner',
				'label' => 'Nama Owner',
				'rules' => 'required',
			),
			array (
				'field' => 'nama_vendor',
				'label' => 'Nama Vendor',
				'rules' => 'required',
			)
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'nama_owner',
				'nama_vendor',
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['created_by'] = $data['updated_by'] = session_pengguna('id');
            $data['created_time']=$data["updated_time"] =date('Y-m-d H:i:s');
			$data['kode']=kode_akun();
			$this->db->insert('vendor', $data);
			
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/vendor/tambah'));
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		$id_akun=$this->input->post('id');
		if ($data != null) {
			$data['updated_time'] = date('Y-m-d H:i:s');
			$data['updated_by'] = session_pengguna('id');
			
			$where = array('id' => $this->input->post('id'));
			
			$this->db->update('vendor', $data, $where);
			
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/vendor/ubah/'.$id_akun));
	}
	
    public function delete($id){
        $penanda=cekTransaksiBeforeDelete("vendor",$akun_id);
		if($penanda==0){
			$data['updated_time'] = date('Y-m-d H:i:s');
			$data['updated_by'] = session_pengguna('id');
			$data['is_deleted'] = "0";
			$where = array('id' => $id);
			
			$this->db->update('vendor', $data, $where);
			redirect(site_url('/master/vendor'));
		}
		else{
			echo "<script>
					alert('Bank ini Digunakan Dalam Transaksi');
					window.location.href='".base_url()."master/vendor';
					</script>";
		}
		
    }

}
