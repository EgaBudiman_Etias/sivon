<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pelanggan_index',
		));
	}
	
	public function datatable()
	{
        $id_vendor=session_pengguna('id_vendor');
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from pelanggan
            where
				is_deleted = '1'
				and (
					kode like ?
                    or tgl_registrasi like ?
                    or nama_kontak like ?
					or no_wa like ?
                    or nama like ?
                    or email like ?
				)
		";
		
		$data_sql = "
			select
				*
				, row_number() over (
					order by
						{$order_by} {$order_direction}
						, kode {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
				, id {$order_direction}
			limit {$offset}, {$num_rows}
		";
                    
		$src = $this->db->query($data_sql, $bindings);
		// echo $this->db->last_query();
        // die();
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'kode' => $row->kode,
				'tgl_registrasi' => $row->tgl_registrasi,
				'nama_kontak' => $row->nama_kontak,
				'no_hp' => $row->no_hp,
				'no_wa' => $row->no_wa,
				'nama' => $row->nama,
                'alamat'=>$row->alamat,
                'email'=>$row->email,
                'id'=>$row->id,
                'no'=>$row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'pelanggan_form',
			'url_aksi' => site_url("/master/pelanggan/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}

	public function tambahbulk($aksi = 'simpanbulk', $data = null)
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pelanggan_bulk_form',
			'url_aksi' => site_url("/master/pelanggan/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->select('*')
			->from('pelanggan')
			->where(array('is_deleted'=>'1','id'=>$id))
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'tgl_registrasi',
				'label' => 'Tanggal Registrasi',
				'rules' => 'required',
			),
			array (
				'field' => 'kode',
				'label' => 'Kode Pelanggan',
				'rules' => 'required',
			),
			array (
				'field' => 'nama',
				'label' => 'Nama Pelanggan',
				'rules' => 'required',
            ),
            array (
				'field' => 'nama_kontak',
				'label' => 'Nama Kontak',
				'rules' => '',
			),
            array (
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => '',
			),
            array (
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required',
			),
			array (
				'field' => 'no_hp',
				'label' => 'Nomor Hp',
				'rules' => 'required|is_numeric',
			),
			array (
				'field' => 'no_wa',
				'label' => 'Nomor Whatsapp',
				'rules' => 'required|is_numeric',
			)
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'tgl_registrasi',
				'kode',
				'nama',
                'nama_kontak',
                'alamat',
                'email',
				'no_hp',
				'no_wa'
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		$id_vendor=session_pengguna('id_vendor');
		if ($data != null) {
			$data['created_by'] = $data['updated_by'] = session_pengguna('id');
            $data['created_time']=$data["updated_time"] =date('Y-m-d H:i:s');
			
            $data['id_vendor']=$id_vendor;
			$this->db->insert('pelanggan', $data);
			
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/pelanggan/tambah'));
	}
	public function simpanbulk_data(){
		$this->load->library('excel');
		$id_vendor=session_pengguna('id_vendor');
		if(isset($_FILES["file"]["name"])){
	
            $path = $_FILES["file"]["tmp_name"];
            $arr_file = explode('.', $_FILES['file']['name']);
            $extension = end($arr_file);
 
            $object = PHPExcel_IOFactory::load($path);

			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
					$kode          		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$nama       		= $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$nama_kontak      	= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $alamat       		= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$email	       		= $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$cellValue          = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $dateValue          = PHPExcel_Shared_Date::ExcelToPHP($cellValue);                       
                    $tgl_registrasi     = date('Y-m-d',$dateValue);  
					$no_hp	       		= $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$no_wa	       		= $worksheet->getCellByColumnAndRow(7, $row)->getValue();
					$created_by			= session_pengguna('id');
					$created_time		= date('Y-m-d H:i:s');
					$updated_by			= session_pengguna('id');
					$updated_time		= date('Y-m-d H:i:s');
					$data[] = array(
						'kode'		    	=>	$kode,
						'nama'				=>	$nama,
						'nama_kontak'		=>	$nama_kontak,
						'alamat'			=>	$alamat,
                        'email'				=>	$email,
                        'tgl_registrasi'	=>	$tgl_registrasi,
						'no_hp'				=>	$no_hp,
						'no_wa'				=>	$no_wa,
						'created_by'		=>	$created_by,
						'created_time'		=>	$created_time,
						'updated_by'		=>	$updated_by,
						'updated_time'		=>	$updated_time,
                    );
                    
				}
            }
            // print_r($data);
            // die();
			
            $simpan=$this->db->insert_batch('pelanggan',$data);
            
			if ($simpan){
                 echo '<script>alert("Data Berhasil di Import !!");window.location.href = "'.base_url().'master/pelanggan/tambahbulk";</script>';
			}else {
				
                echo '<script>alert("Data gagal di Import !!");window.location.href = "'.base_url().'master/pelanggan/tambahbulk";</script>';
			}
		} else {
			
            echo '<script>alert("Data gagal di Import !!Tipe File Tidak Sesuai");window.location.href = "'.base_url().'master/pelanggan/tambahbulk";</script>';
		}
	}
	public function ubah_data()
	{
		$data = $this->_data_form();
		$id=$this->input->post('id');
		if ($data != null) {
			$data['updated_time'] = date('Y-m-d H:i:s');
			$data['updated_by'] = session_pengguna('id');
			
			$where = array('id' => $this->input->post('id'));
			
			$this->db->update('pelanggan', $data, $where);
			
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/master/pelanggan/ubah/'.$id));
	}
	
    public function delete($id){
        $penanda=cekTransaksiBeforeDelete("pelanggan",$id);
		if($penanda==0){
			$data['updated_time'] = date('Y-m-d H:i:s');
			$data['updated_by'] = session_pengguna('id');
			$data['is_deleted'] = "0";
			$where = array('id' => $id);
			
			$this->db->update('pelanggan', $data, $where);
			redirect(site_url('/master/pelanggan'));
		}
		else{
			echo "<script>
					alert('Pelanggan ini Digunakan Dalam Transaksi');
					window.location.href='".base_url()."master/pelanggan';
					</script>";
		}
		
    }

}
