<div class="col-md-10 offset-md-1 p-0">
	<div class="card">
		<div class="card-header">
			Master Pelanggan
			<a href="pelanggan/tambah" class="btn btn-primary btn-sm btn-header">
				<i class="ti ti-write"></i> Tambah Data
			</a>
			<a href="pelanggan/tambahbulk" class="btn btn-danger btn-sm btn-header">
				<i class="ti ti-write"></i> Tambah Data (excel)
			</a>
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Kode</th>
						<th>Tanggal Registrasi</th>
						<th>Nama Pelanggan</th>
						<th>Nama Kontak</th>
						<th>Nama Whatsapp</th>
						<th>Email</th>
                        
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
	function init_datatable()
	{
		datatable = $('#datatable').DataTable ({
			'bInfo': true,
			'serverSide': true,
			'serverMethod': 'post',
			'ajax': '<?php echo site_url('/master/pelanggan/datatable'); ?>',
			'order': [[ 2, 'asc' ]],
			'fixedHeader': true,
			'columns': [
				{
					data: function (row, type, val, meta) {
						return '' +
							'<a class="btn btn-action btn-primary" href="pelanggan/ubah/'+row.id+'">'+
								'<i class="ti ti-pencil-alt"></i>'+
							'</a>&nbsp;'+
							'<a class="btn btn-action btn-danger btn-delete" href="pelanggan/delete/'+row.id+'">'+
								'<i class="ti ti-trash"></i>'+
							'</a>';
					},
					orderable: false,
					className: 'dt-body-center'
				},
				{ data: 'no', orderable: false },
				{ data: 'kode' },
				{ data: 'tgl_registrasi' },
				{ data: 'nama' },
				{ data: 'nama_kontak' },
				{ data: 'no_wa' },
				{ data: 'email' },
				
			]
		});
	}

	$().ready(function() {
		
		init_datatable();
		
	});
</script>