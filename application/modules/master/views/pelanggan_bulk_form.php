<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Pelanggan 
			<a href="<?php echo site_url('/master/pelanggan'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" enctype="multipart/form-data" > 
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Template Excell
					</label>
					<div class="col-sm-6 pr-sm-0">
						
						<label class="col-sm-12 col-form-label pr-sm-0 ">
							<span class="text-danger"></span><a href="<?=base_url()?>assets/file_upload/template_pelanggan.xlsx" target="_blank"> Download Template</a>
						</label>
						
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> File Upload
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="file" class="form-control" name="file" >
					</div>
				</div>
				
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>