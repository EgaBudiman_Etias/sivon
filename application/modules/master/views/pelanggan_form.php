<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Pelanggan 
			<a href="<?php echo site_url('/master/pelanggan'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="id" value="<?php if ($data != null) echo $data->id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kode Pelanggan
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control " name="kode" value="<?php if ($data != null) echo $data->kode; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Tanggal Registrasi
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control tanggalan" name="tgl_registrasi" value="<?php if ($data != null) echo $data->tgl_registrasi; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Pelanggan
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="nama" value="<?php if ($data != null) echo $data->nama; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Nama Kontak
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="nama_kontak" value="<?php if ($data != null) echo $data->nama_kontak; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Email
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="email" value="<?php if ($data != null) echo $data->email; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No Hp
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="no_hp" value="<?php if ($data != null) echo $data->no_hp; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> No Whatsapp
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="no_wa" value="<?php if ($data != null) echo $data->no_wa; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Alamat
					</label>
					<div class="col-sm-6 pr-sm-0">
						<textarea name="alamat" cols="30" rows="5" class="form-control"><?php if ($data != null) echo $data->alamat; ?></textarea>
					</div>
				</div>
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>