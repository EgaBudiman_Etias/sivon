<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Produk 
			<a href="<?php echo site_url('/master/produk'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="id" value="<?php if ($data != null) echo $data->id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kode Produk
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="kode_produk" value="<?php if ($data != null) echo $data->kode_produk; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama produk
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="nama" value="<?php if ($data != null) echo $data->nama; ?>">
					</div>
				</div>
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Harga
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control angka" name="harga" value="<?php if ($data != null) echo $data->harga; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Deskripsi
					</label>
					<div class="col-sm-6 pr-sm-0">
						<textarea name="deskripsi" cols="30" rows="5" class="form-control"><?php if ($data != null) echo $data->deskripsi; ?></textarea>
					</div>
				</div>
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>