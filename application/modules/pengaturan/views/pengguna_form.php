<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Pengguna
			<a href="<?php echo site_url('/pengaturan/pengguna'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php if ($data != null) echo $data->id; ?>">
				<input type="hidden" name="id_grup" value="<?php if ($data != null) echo $data->id_pengguna_grup; ?>" id='id_grup'>
				<input type="hidden" id="id_vendor" name="id_vendor" value="<?php if ($data != null) echo $data->id_vendor; ?>">
				<?php
					$id_vendor=session_pengguna('id_vendor');
					
					if($id_vendor=='1'){
				?>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Vendor
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select class="form-control select2" onchange="kirimId()" name="id_vendor2" id="id_vendor2">
							<option value="">- Pilih Vendor -</option>
							<?=options_vendor(!empty($data)?$data->id_vendor:'')?>
						</select>
					</div>
					
				</div>
				<?php
					}else{
				?>
				<input type="hidden" class="form-control" id='id_vendor_fix' onchange="" name="id_vendor_fix" value="<?=$id_vendor?>">
				<?php
					}
				?>
				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nama Pengguna
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="nama_pengguna" value="<?php if ($data != null) echo $data->nama_pengguna; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Alamat Surel
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="surel" value="<?php if ($data != null) echo $data->surel; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<?php if (! isset($data->id)): ?>
						<span class="text-danger">*</span>
						<?php endif; ?>
						Kata Sandi
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="kata_sandi" placeholder="<?php if (isset($data->id)) echo 'Isi jika ingin mengubah kata sandi'; ?>">
					</div>
				</div>
				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">No. WhatsApp</label>
					
					<div class="col-sm-3 pr-sm-0">
						<input type="text" class="form-control" name="no_wa" value="<?php if ($data != null) echo $data->no_wa; ?>">
					</div>
				</div>
				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Grup Pengguna
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select class="form-control select2" id="pengguna_grup" name="id_pengguna_grup">
							<option value="">- Pilih Grup Pengguna -</option>
							
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

