<div class="col-md-10 offset-md-1 p-0">
	<div class="card">
		<div class="card-header">
			Pengaturan Hak Akses
			<span class="text-divider">&nbsp;&rsaquo;&nbsp;</span>
			<span class="text-primary"><?php echo $data->nama; ?></span>
			
			<a class="btn btn-outline-primary btn-sm float-right" href="<?php echo site_url('/pengaturan/pengguna-grup'); ?>">
				<i class="ti ti-back-left"></i> Kembali ke Data Grup Pengguna
			</a>
		</div>
		<div class="card-body">
			<form method="post" action="<?php echo site_url('/pengaturan/pengguna-grup/simpan-akses/'.$data->id); ?>">
				<button type="submit" class="btn btn-primary btn-sm mb-3">
					<i class="ti ti-save"></i>
					Simpan Data Hak Akses
				</button>
				
				<ul class="nav nav-tabs" id="TabMenu">
					<?php foreach ($menu as $id_menu => $m): ?>
					<li class="nav-item">
						<a class="nav-link<?php echo $m['kode'] == 'dasbor' ? ' active': '' ?>" id="<?php echo $m['kode'] ?>-tab" data-toggle="tab" href="#<?php echo $m['kode']; ?>" role="tab" aria-controls="<?php echo $m['kode']; ?>" aria-selected="<?php echo $m['kode'] == 'dasbor' ? 'true' : 'false'; ?>"><?php echo $m['teks']; ?></a>
					</li>
					<?php endforeach; ?>
				</ul>
				
				<div class="tab-content p-3 pt-4" id="TabMenuContent">
					<?php foreach ($menu as $id_menu => $m): ?>
					<div class="tab-pane fade<?php if ($m['kode'] == 'dasbor') echo ' show active'; ?>" id="<?php echo $m['kode'] ?>" role="tabpanel" aria-labelledby="<?php echo $m['kode']; ?>-tab">
						<table class="table-cell">
							<tr style="background:#f5f5f5">
								<td>
									<div class="custom-control custom-checkbox">
										<span class="ml-2"></span>
										<input class="custom-control-input all" data-kode="<?php echo $m['kode']; ?>" type="checkbox" id="menu_<?php echo $m['kode']; ?>" <?php if ($m['kode'] == 'dasbor') echo 'checked onclick="return false"'; ?> name="id_menu[<?php echo $id_menu; ?>]" value="<?php echo $id_menu; ?>">
										<label class="custom-control-label" for="menu_<?php echo $m['kode']; ?>"></label>
									</div>
								</td>
								<td width="300px">
									<?php if (count($m['submenu']) == 0): ?>
									Beri akses pada menu ini
									<?php else: ?>
									Beri akses semua menu di bawah ini
									<?php endif; ?>
								</td>
							</tr>
							<?php foreach ($m['submenu'] as $sm): ?>
							<?php $id_sub_menu = $sm['id_menu']; ?>
							<tr>
								<td>
									<div class="custom-control custom-checkbox">
										<span class="ml-2"></span>
										<input class="custom-control-input <?php echo $m['kode']; ?>" type="checkbox" id="menu_<?php echo $id_sub_menu; ?>" name="id_menu[<?php echo $id_sub_menu; ?>]" value="<?php echo $id_sub_menu; ?>" <?php if ($sm['id_pengguna_grup'] != null) echo 'checked'; ?>>
										<label class="custom-control-label" for="menu_<?php echo $id_sub_menu; ?>"></label>
									</div>
								</td>
								<td><?php echo $sm['teks']; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
					<?php endforeach; ?>
				</div>
			</form>
		</div>
	</div>
</div>

<style>
.table-cell tr td {
	color: #333;
	font-size: 13px;
}
</style>

<script type="text/javascript">

$().ready(function() {
	
	$('.all').click(function() {
		
		var kelas = $(this).data('kode');
		var is_checked = $(this).is(':checked');
		
		$('.'+kelas).attr('checked', is_checked);
	});
	
});

</script>