<div class="col-md-10 offset-md-1 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Manajemen Pengguna
			<a href="<?php echo site_url('/pengaturan/pengguna/tambah'); ?>" class="btn btn-primary btn-sm btn-header">
				<i class="ti ti-write"></i> Tambah Data
			</a>
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Nama Pengguna</th>
						<th>Kode Vendor</th>
						<th>Nama Vendor</th>
						<th>Surel</th>
						<th>No. WA</th>
						<th>Grup Pengguna</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
function init_datatable()
{
	datatable = $('#datatable').DataTable ({
		'bInfo': true,
		'pageLength': 25,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': '<?php echo site_url('/pengaturan/pengguna/datatable'); ?>',
		'order': [[ 3, 'asc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="'+site_url+'pengaturan/pengguna/ubah/'+row.id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;';
                },
				orderable: false,
				className: 'dt-body-center'
			},
			{ data: 'nomor', orderable: false },
			{ data: 'nama' },
			{ data: 'kode_vendor' },
			{ data: 'nama_vendor' },
			{ data: 'surel' },
			{ data: 'no_wa' },
			{ data: 'pengguna_grup' }
		]
	});
}

$().ready(function() {
	
	init_datatable();
	
});
</script>