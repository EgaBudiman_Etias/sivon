<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_grup extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_grup_index',
		));
	}
	
	public function datatable()
	{
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%");
		
		$base_sql = "
			from pengguna_grup
			where
				is_hapus = 0
				and nama like ?
		";
		
		$data_sql = "
			select
				*
				, row_number() over (
					order by {$order_by} {$order_direction}
				  ) as nomor
			{$base_sql}
			order by {$order_by} {$order_direction}
			limit {$offset}, {$num_rows}
		";
		$src = $this->db->query($data_sql, $bindings);
		
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'id' => $row->id,
				'nama' => $row->nama,
				'urutan' => $row->urutan,
				'nomor' => $row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _menu($id_pengguna_grup)
	{
		$sql = "
			select
				a.id as id_menu
				, a.id_menu_induk
				, a.kode
				, a.nomor
				, a.teks
				, b.id_pengguna_grup
			from (
				select *
				from menu
				where is_hapus = 0
			) as a
			left join (
				select *
				from pengguna_grup_menu
				where
					is_hapus = 0
					and id_pengguna_grup = ?
			) as b
				on a.id = b.id_menu
			order by a.nomor
		";
		$src = $this->db->query($sql, array($id_pengguna_grup));
		
		$menu = array();
		
		foreach ($src->result() as $row) {
			
			if ($row->id_menu_induk == null) {
				$menu[$row->id_menu] = array (
					'kode' => $row->kode,
					'nomor' => $row->nomor,
					'teks' => $row->teks,
					'id_pengguna_grup' => $row->id_pengguna_grup,
					'submenu' => array(),
				);
			}
			else {
				$menu[$row->id_menu_induk]['submenu'][] = array (
					'id_menu' => $row->id_menu,
					'kode' => $row->kode,
					'nomor' => $row->nomor,
					'teks' => $row->teks,
					'id_pengguna_grup' => $row->id_pengguna_grup,
				);
			}
		}
		
		return $menu;
	}
	
	public function akses($id)
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('pengguna_grup')
			->where('is_hapus', 0)
			->where('id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$data = $src->row();
		$menu = $this->_menu($id);
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_grup_akses',
			'data' => $data,
			'menu' => $menu,
		));
	}
	
	public function simpan_akses($id)
	{
		$id_menu_arr = $this->input->post('id_menu');
		$id_menu_str = implode(',', $id_menu_arr);
		
		#
		# AMBIL ID_MENU_INDUK
		#
		$sql = "
			select distinct id_menu_induk
			from menu
			where
				id in ({$id_menu_str})
				and id_menu_induk is not null
		";
		$src = $this->db->query($sql, array($id));
		
		$this->db->trans_start();
		
		#
		# TAMBAHKAN ID_MENU_INDUK KE ID_MENU_ARR
		#
		foreach ($src->result() as $row) {
			$id_menu_arr[$row->id_menu_induk] = $row->id_menu_induk;
		}
		
		sort($id_menu_arr);
		$id_menu_str = implode(',', $id_menu_arr);
		
		# HAPUS ID_MENU YANG SUDAH TIDAK DIGUNAKAN
		$sql = "
			update pengguna_grup_menu
			set
				is_hapus = 1
				, waktu_ubah = ?
				, id_pengguna_ubah = ?
			where
				id_menu not in ({$id_menu_str})
				and id_pengguna_grup = ?
		";
		$this->db->query($sql, array(date('Y-m-d H:i:s'), session_pengguna('id'), $id));
		
		# TAMBAHKAN ID MENU
		foreach ($id_menu_arr as $id_menu) {
			$sql = "
				insert into pengguna_grup_menu (
					id_pengguna_grup
					, id_menu
					, id_pengguna_buat
					, id_pengguna_ubah
				)
				select * from (
					select
						{$id} as id_pengguna_grup
						, {$id_menu} as id_menu
						, ".session_pengguna('id')." as id_pengguna_buat
						, ".session_pengguna('id')." as id_pengguna_ubah
				) as temp
				where not exists (
					select id_menu
					from pengguna_grup_menu
					where
						id_menu = {$id_menu}
						and id_pengguna_grup = {$id}
						and is_hapus = 0
				) limit 1
			";
			$this->db->query($sql);
		}
		
		$this->db->trans_complete();
		
		# JIKA SEDANG LOGIN, UPDATE MENU
		if ($id == session_pengguna('id_pengguna_grup')) {
			
			$this->load->model('pengguna/pengguna_grup_model');
			
			$menu = $this->pengguna_grup_model->menu($id);
			
			$this->session->set_userdata('menu', $menu);
		}
		
		redirect($this->agent->referrer());
	}
	
}
