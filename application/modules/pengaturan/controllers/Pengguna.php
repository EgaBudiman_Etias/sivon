<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_index',
		));
	}
	
	public function datatable()
	{
		$id_vendor=session_pengguna('id_vendor');
		$draw = $this->input->post('draw');
		$offset = $this->input->post('start');
		$num_rows = $this->input->post('length');
		$order_index = $_POST['order'][0]['column'];
		$order_by = $_POST['columns'][$order_index]['data'];
		$order_direction = $_POST['order'][0]['dir'];
		$keyword = $_POST['search']['value'];
		
		$bindings = array("%{$keyword}%", "%{$keyword}%","%{$keyword}%");
		
		$base_sql = "
			from pengguna as a
			join pengguna_grup as b on
				a.id_pengguna_grup = b.id
				and b.is_deleted = '1'
			Join vendor as c on
				a.id_vendor=c.id
				and c.is_deleted='1'
			where
				a.is_deleted = '1'
				and (
					a.nama_pengguna like ?
					or b.nama like ?
					or c.nama_vendor like ?
				)
		";
		if($id_vendor!='1'){
			$base_sql.=" AND a.id_vendor='$id_vendor'";
		}
		
		$data_sql = "
			select
				a.*,c.kode as kode_vendor
				, b.nama as pengguna_grup,c.nama_vendor as nama_vendor
				, row_number() over (
					order by {$order_by} {$order_direction}
				  ) as nomor
			{$base_sql}
			order by
				{$order_by} {$order_direction}
			limit {$offset}, {$num_rows}
		";
		$src = $this->db->query($data_sql, $bindings);
		
		$count_sql = "
			select count(*) AS total
			{$base_sql}
		";
		$total_records = $this->db->query($count_sql, $bindings)->row('total');
		
		$data = array();
		
		foreach ($src->result() as $row) {
			$data[] = array (
				'id' => $row->id,
				'nama' => $row->nama_pengguna,
				'surel' => $row->surel,
				'no_wa' => $row->no_wa,
				'pengguna_grup' => $row->pengguna_grup,
				'nama_vendor'=>$row->nama_vendor,
				'kode_vendor'=>$row->kode_vendor,
				'nomor' => $row->nomor,
			);
		}
		
		$response = array (
			'draw' => intval($draw),
			'iTotalRecords' => $src->num_rows(),
			'iTotalDisplayRecords' => $total_records,
			'aaData' => $data,
		);
		
		echo json_encode($response);
	}
	
	private function _form($aksi = 'tambah', $data = null)
	{
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
			$data->foto_ktp = $data->file_foto_ktp;
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_form',
			'url_aksi' => site_url("/pengaturan/pengguna/{$aksi}-data"),
			'data' => $data,
		));
	}
	
	public function tambah()
	{
		$this->_form();
	}
	
	public function ubah($id = '')
	{
		if ( ! $this->agent->referrer()) {
			show_404();
		}
		
		$src = $this->db
			->from('pengguna')
			->where('is_deleted', '1')
			->where('id', $id)
			->get();
		
		if ($src->num_rows() == 0) {
			show_404();
		}
		
		$this->_form('ubah', $src->row());
	}
	
	private function _data_form()
	{
		$validasi = array (
			array (
				'field' => 'nama_pengguna',
				'label' => 'Nama Pengguna',
				'rules' => 'required',
			),
			array (
				'field' => 'surel',
				'label' => 'Alamat Surel',
				'rules' => 'required',
			),
			array (
				'field' => 'id_pengguna_grup',
				'label' => 'Grup Pengguna',
				'rules' => 'required',
			),
			array (
				'field' => 'id_vendor',
				'label' => 'Vendor',
				'rules' => 'required',
			),
		);
		
		if ($this->input->post('id') == '') {
			$validasi[] = array (
				'field' => 'kata_sandi',
				'label' => 'Kata Sandi',
				'rules' => 'required',
			);
		}
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'nama_pengguna',
				'surel',
				'no_wa',
				'id_vendor',
				'id_pengguna_grup',
			);
			
			return data_post($kunci_data);
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
			return null;
		}
	}
	
	private function _upload_ktp()
	{
		if ($_FILES['foto_ktp'] && is_uploaded_file($_FILES['foto_ktp']['tmp_name'])) {
			$konfigurasi_unggah = array (
				'upload_path' => './uploads/pengguna/',
				'allowed_types' => 'jpg|jpeg|png|gif|bmp',
				'max_size' => 20480, # 20 MB
				'max_width' => 10240,
				'max_height' => 7680,
				'file_ext_tolower' => true,
				'encrypt_name' => true,
			);
			
			$this->load->library('upload', $konfigurasi_unggah);
			
			$berhasil_unggah = $this->upload->do_upload('foto_ktp');
			
			if ($berhasil_unggah) {
				$data_file = $this->upload->data();
				return $data_file['file_name'];
			}
			else {
				$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
				$this->session->set_flashdata('validation_errors', $this->upload->display_errors());
				$this->session->set_flashdata('data_form', (object) $this->input->post());
				redirect($this->agent->referrer());
			}
		}
		
		return null;
	}
	
	public function tambah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['password'] = hash_kata_sandi(trim($this->input->post('kata_sandi')));
			$data['created_by'] = $data['updated_by'] = session_pengguna('id');
			
			$this->db->insert('pengguna', $data);
			
			$this->session->set_flashdata('status_simpan', 'ok');
			redirect(site_url('/pengaturan/pengguna'));
		}
		else {
			redirect(site_url('/pengaturan/pengguna/tambah'));
		}
	}
	
	public function ubah_data()
	{
		$data = $this->_data_form();
		
		if ($data != null) {
			$data['updated_time'] = date('Y-m-d H:i:s');
			$data['updated_by'] = session_pengguna('id');
			
			$kata_sandi = $this->input->post('kata_sandi');
			
			if ($kata_sandi != '') {
				$data['password'] = hash_kata_sandi($kata_sandi);
			}
			
			$where = array('id' => $this->input->post('id'));
			
			$this->db->update('pengguna', $data, $where);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		
		redirect(site_url('/pengaturan/pengguna'));
	}
	
}
