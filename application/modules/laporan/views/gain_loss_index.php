<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Gain Loss
		</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Akun</label>
                        <select class="form-control akun" name="akun_id" onchange="getSaham()" id="akun_id" data-placeholder="Pilih Akun" style="width:100%" >
                            <option value="all">--Semua Data--</option>
                            <?=options_akun()?>
                        </select>    
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Saham</label>
                        <select class="form-control select2" name="sham_id" id="sham_id" data-placeholder="Pilih Saham" style="width:100%" >
                            <option value="0"></option>
                            
                        </select>    
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Dari Tanggal</label>
                        <input type="text" class="form-control tanggalan" name="dari_tanggal" id="dari_tanggal" value="<?=date('Y-m-01')?>" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Hingga Tanggal</label>
                        <input type="text" class="form-control tanggalan" name="hingga_tanggal" id="hingga_tanggal" value="<?=date('Y-m-t')?>" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <button class="btn btn-primary" style="margin-top:23px;" onclick="klikFilter()" id="btn-show">Tampilkan</button>
                    
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Total Gain Loss</label>
                        <div  id="saldoawal" class="tebel">
                            0
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="card-body">
            
			<table class="table table-striped table-bordered" id="tablelaporan3">
				<thead>	
					<tr>
						
						<th width="10px">No.</th>
						<th>Tanggal</th>
						<th>Akun</th>
						<th>Saham</th>
                        <th>Lot Jual</th>
                        <th>Shares Jual</th>
                        <th>Harga Jual /Shares</th>
                        
                        <th>Gain/Loss</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
    function getSaham(){
        var akun_id1=$("#akun_id").val();
        $("#sham_id").empty();
        if(akun_id1!="all"){
            $.ajax({
                url: '<?=base_url()?>master/saham/getSaham',
                type: 'POST',
                data: {'akun_id':akun_id1},
                success: function(msg){
                    console.log(msg);
                    msg.forEach(element => {
                        $("#sham_id").append("<option value='" + element.sham_id + "'>" + element.sham_kode +  " - "+element.sham_nama+"</option>");
                    });
                }
            });
        };
    }
    function klikFilter(){
        var akun_id1=$("#akun_id").val();
        var saham_id=$("#sham_id").val();
        
        if(saham_id==null){
            saham_id="0";
        }
        console.log(saham_id);
        if(akun_id1==""){
            alert("Pilih Akun Terlebih Dahulu");
            return;
        }
        var dari_tanggal=$("#dari_tanggal").val();
        var hingga_tanggal=$("#hingga_tanggal").val();
        var table = $('#tablelaporan3').DataTable();

        table.destroy();
        table =  $('#tablelaporan3').DataTable ({
            "processing": true,
            "serverSide": true,
            "paging" :false,
            "bInfo" : false,
            "searching": false,
            "ajax":
            {
                "url": "<?php echo base_url()?>laporan/gainloss/datatable/"+akun_id1+'/'+saham_id+'/'+dari_tanggal+'/'+hingga_tanggal, 
                "type": "POST"
            },
            'columns': [
                { data: 'no', orderable: false },
                { data: 'tgl', orderable: false },
                { data: 'akun',orderable: false },
                { data: 'saham',orderable: false},
                { data: 'lot',className: 'dt-body-right',orderable: false},
                { data: 'shares',className: 'dt-body-right',orderable: false},
                { data: 'harga_jual',className: 'dt-body-right',orderable: false},
                { data: 'gl',className: 'dt-body-right',orderable: false},
		    ],
            rowCallback: function(row, data, index){
                // console.log(eq[2]);
                if(data["gl2"]<0){
                    $('td', row).eq(7).addClass('highlight');
                }
                
            },
            dom: 'Blfrtip',
            buttons: [
                
                {
                    extend: 'pdfHtml5',
                    orientation: 'potrait',
                    pageSize: 'A4'
                },
                {
                    extend: 'excel',
                    charset: 'UTF-8',
                    exportOptions: {
                        orthogonal: 'sort'
                    },          
                }
            ],
            
        });
        $.ajax({
            url: '<?=base_url()?>laporan/gainloss/getData/',
            type: 'POST',
            data: {'akun_id':akun_id1,'saham_id':saham_id,'dari_tanggal':dari_tanggal,'hingga_tanggal':hingga_tanggal},
            success: function(msg){
                parse = $.parseJSON(msg);
                console.log(parse['gainlos']);
                $('#saldoawal').text(parse['gainlos']);
                
                if(parse['tanda']=="min"){
                    $('#saldoawal').last().addClass('warnanyamerah');
                    
                }else{
                    $('#saldoawal').last().addClass('warnanyabiru');
                }
                
            }
        });
    };
</script>