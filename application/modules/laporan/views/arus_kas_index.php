<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Arus Kas
		</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Akun</label>
                        <select class="form-control akun" name="devi_akun_id2" id="akun_id" data-placeholder="Pilih Akun" style="width:100%" >
                            <option value=""></option>
                            <?=options_akun()?>
                        </select>    
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Dari Tanggal</label>
                        <input type="text" class="form-control tanggalan" name="dari_tanggal" id="dari_tanggal" value="<?=date('Y-m-01')?>" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Hingga Tanggal</label>
                        <input type="text" class="form-control tanggalan" name="hingga_tanggal" id="hingga_tanggal" value="<?=date('Y-m-t')?>" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <button class="btn btn-primary" style="margin-top:23px;" onclick="klikFilter()" id="btn-show">Tampilkan</button>
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Saldo Awal</label>
                        <div  id="saldoawal" class="tebel">
0
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Mutasi</label>
                        <div id="mutasi" class="tebel">0</div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Saldo Akhir</label>
                        <!-- <label for="" id="saldoakhir"></label> -->
                        <div id="saldoakhir" class="tebel">0</div>
                    </div>
                </div>
                
            </div>
        </div>
		<div class="card-body">
            
			<table class="table table-striped table-bordered" id="tablelaporan1">
				<thead>	
					<tr>
						
						<th width="10px">No.</th>
						<th>Tanggal</th>
						<th>Uraian</th>
						<th>Nominal</th>
                        <th>Saldo</th>
                        
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
    
    function klikFilter(){
        var akun_id1=$("#akun_id").val();
        console.log(akun_id1);
        if(akun_id1==""){
            alert("Pilih Akun Terlebih Dahulu");
            return;
        }
        var dari_tanggal=$("#dari_tanggal").val();
        var hingga_tanggal=$("#hingga_tanggal").val();
        var table = $('#tablelaporan1').DataTable();

        table.destroy();
        table =  $('#tablelaporan1').DataTable ({
            "processing": true,
            "serverSide": true,
            "paging" :false,
            "bInfo" : false,
            "searching": false,
            "ajax":
            {
                "url": "<?php echo base_url()?>laporan/aruskas/datatable/"+akun_id1+'/'+dari_tanggal+'/'+hingga_tanggal, 
                "type": "POST"
            },
            'columns': [
                { data: 'no', orderable: false },
                { data: 'uang_tgl', orderable: false },
                { data: 'uang_jns_transaksi',orderable: false },
                { data: 'uang_nominal',className: 'dt-body-right' ,orderable: false},
                { data: 'saldo',className: 'dt-body-right',orderable: false},
		    ],
            rowCallback: function(row, data, index){
                // console.log(eq[2]);
                if(data["tanda"]=="min"){
                    $('td', row).eq(3).addClass('highlight');
                }
                if(data["tanda2"]=="min"){
                    $('td', row).eq(4).addClass('highlight');
                }
            },
            dom: 'Blfrtip',
            buttons: [
                
                {
                    extend: 'pdfHtml5',
                    orientation: 'potrait',
                    pageSize: 'A4'
                },
                {
                    extend: 'excel',
                    charset: 'UTF-8',
                    exportOptions: {
                        orthogonal: 'sort'
                    },          
                }
            ],
            
        });
        
        $.ajax({
            url: '<?=base_url()?>laporan/aruskas/getData/',
            type: 'POST',
            data: {'akun_id':akun_id1,'dari_tanggal':dari_tanggal,'hingga_tanggal':hingga_tanggal},
            success: function(msg){
                parse = $.parseJSON(msg);
                console.log(msg);
                $('#saldoawal').text(parse['saldoawal']);
                $('#mutasi').text(parse['saldomutasi']);
                $('#saldoakhir').text(parse['saldoakhir']);
                if(parse['tanda2']=="min"){
                    $('#mutasi').last().addClass('warnanyamerah');
                    
                }else{
                    $('#mutasi').last().addClass('warnanyabiru');
                }
                
            }
        });
    };
</script>