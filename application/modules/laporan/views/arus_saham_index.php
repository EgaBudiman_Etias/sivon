<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Arus Saham
		</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Akun</label>
                        <select class="form-control akun" onchange="getSaham()" name="akun_id" id="akun_id" data-placeholder="Pilih Akun" style="width:100%" >
                            <option value=""></option>
                            <?=options_akun()?>
                        </select>    
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Saham</label>
                        <select class="form-control select2" name="sham_id" id="sham_id" data-placeholder="Pilih Saham" style="width:100%" >
                            <option value=""></option>
                            
                        </select>    
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Dari Tanggal</label>
                        <input type="text" class="form-control tanggalan" name="dari_tanggal" id="dari_tanggal" value="<?=date('Y-m-01')?>" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Hingga Tanggal</label>
                        <input type="text" class="form-control tanggalan" name="hingga_tanggal" id="hingga_tanggal" value="<?=date('Y-m-t')?>" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    
                    <button class="btn btn-primary" style="margin-top:23px;" onclick="klikFilter()" id="btn-show">Tampilkan</button>
                </div>
            </div>
            
           
        </div>
		<div class="card-body">
            
			<table class="table table-striped table-bordered" id="tablelaporan2">
				<thead>	
					<tr>
						<th width="10px">No.</th>
						<th>Tanggal</th>
						<th>Uraian</th>
						<th>Jumlah Lot</th>
                        <th>Jumlah Shares</th>
                        <th>Saldo Lot</th>
                        <th>Saldo Shares</th>
                        <th>Harga Beli / Jual per Share </th>
                        <th>Harga Beli / Jual Net</th>
                        <th>Saldo Saham</th>
                        <th>Harga Avg Before</th>
                        <th>Harga Avg After</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
    function getSaham(){
        var akun_id1=$("#akun_id").val();
        $("#sham_id").empty();
        $.ajax({
            url: '<?=base_url()?>master/saham/getSaham',
            type: 'POST',
            data: {'akun_id':akun_id1},
            success: function(msg){
                console.log(msg);
                msg.forEach(element => {
                    $("#sham_id").append("<option value='" + element.sham_id + "'>" + element.sham_kode +  " - "+element.sham_nama+"</option>");
                });
            }
        });
    }
    function klikFilter(){
        var akun_id1=$("#akun_id").val();
        var saham_id=$("#sham_id").val();
        var dari_tanggal=$("#dari_tanggal").val();
        var hingga_tanggal=$("#hingga_tanggal").val();
        var table = $('#tablelaporan2').DataTable();
        if(akun_id1==""){
            alert('PILIH Akun Dahulu');
            return;
        };
        if(saham_id==""){
            alert('PILIH Saham Dahulu');
            return;
        }
        table.destroy();
        table =  $('#tablelaporan2').DataTable ({
            "processing": true,
            "serverSide": true,
            "paging" :false,
            "bInfo" : false,
            "searching": false,
            "ajax":
            {
                "url": "<?php echo base_url()?>laporan/arussaham/datatable/"+akun_id1+'/'+saham_id+'/'+dari_tanggal+'/'+hingga_tanggal, 
                "type": "POST"
            },
            'columns': [
                { data: 'no', orderable: false },
                { data: 'tanggal',orderable: false },
                { data: 'uraian',orderable: false },
                { data: 'jumlah_lot',className: 'dt-body-right' ,orderable: false},
                { data: 'jumlah_shares',className: 'dt-body-right',orderable: false},
                { data: 'saldo_lot',className: 'dt-body-right',orderable: false},
                { data: 'saldo_shares',className: 'dt-body-right',orderable: false},
                { data: 'harga',className: 'dt-body-right',orderable: false},
                { data: 'harga_net',className: 'dt-body-right',orderable: false},
                { data: 'saldo_saham',className: 'dt-body-right',orderable: false},
                { data: 'avg_before',className: 'dt-body-right',orderable: false},
                { data: 'avg_after',className: 'dt-body-right',orderable: false},
		    ],
            rowCallback: function(row, data, index){
                if(data["harga_net2"]<0){
                    $('td', row).eq(8).addClass('highlight');
                }
                if(data["jumlah_lot2"]<0){
                    $('td', row).eq(4).addClass('highlight');
                    $('td', row).eq(3).addClass('highlight');
                    
                }
                // if(data["tanda2"]=="min"){
                //     $('td', row).eq(5).addClass('highlight');
                // }
            },
        });
        
        $.ajax({
            url: '<?=base_url()?>laporan/arussaham/getData/',
            type: 'POST',
            data: {'akun_id':akun_id1,'saham_id':saham_id,'dari_tanggal':dari_tanggal,'hingga_tanggal':hingga_tanggal},
            success: function(msg){
                parse = $.parseJSON(msg);
                console.log(msg);
                $('#saldoawal').text(parse['saldoawal']);
                $('#mutasi').text(parse['saldomutasi']);
                $('#saldoakhir').text(parse['saldoakhir']);
                if(parse['tanda2']=="min"){
                    $('#mutasi').last().addClass('warnanyamerah');
                    
                }else{
                    $('#mutasi').last().addClass('warnanyabiru');
                }
                
            }
        });
    };
</script>