<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aruskas extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'arus_kas_index',
		));
	}
	
	public function datatable($akun_id="",$dari_tanggal,$hingga_tanggal)
	{
		
        $query="";
        
        $query="select coalesce(sum(uang_nominal),0) as uang_nominal from keuangan where uang_akun_id='$akun_id' AND uang_tgl < '".$dari_tanggal."' and uang_jns_transaksi!='saldoawal'";
        $saldosebelum=$this->db->query($query)->row()->uang_nominal;
        $data=array();
        $saldo=$saldosebelum;

        $query="SELECT '0.awal' as uang_id,swal_akun_id as uang_akun_id,swal_tgl_saldo_awal as uang_tgl,
                'Saldo Awal' as uang_jns_transaksi,swal_jumlah_saldo as uang_nominal
                FROM saldo_awal WHERE swal_akun_id='$akun_id'";
        $saldowahasil=$this->db->query($query)->result();
        $query="SELECT * FROM (
                    SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
                    FROM keuangan
                    JOIN pembelian on(pmbl_id=uang_transaksi_id)
                    where uang_jns_transaksi='pembelian' and pmbl_is_deleted='1' and uang_akun_id='$akun_id'

                    UNION

                    SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
                    FROM keuangan
                    JOIN penjualan on(pnjl_id=uang_transaksi_id)
                    where uang_jns_transaksi='penjualan' and pnjl_is_deleted='1' and uang_akun_id='$akun_id'

                    UNION
                    
                    SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
                    FROM keuangan
                    JOIN setoran on(stor_id=uang_transaksi_id)
                    where uang_jns_transaksi='setoran'  and stor_is_deleted='1' and uang_akun_id='$akun_id'

                    UNION

                    SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
                    FROM keuangan
                    JOIN penarikan on(trik_id=uang_transaksi_id)
                    where uang_jns_transaksi='penarikan' and trik_is_deleted='1' and uang_akun_id='$akun_id'

                    UNION

                    SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
                    FROM keuangan
                    JOIN deviden on(devi_id=uang_transaksi_id)
                    where uang_jns_transaksi='deviden' and devi_is_deleted='1' and uang_akun_id='$akun_id'
                    ) as a
                WHERE a.uang_tgl 
                between '$dari_tanggal' and '$hingga_tanggal' ORDER BY uang_tgl,uang_id asc";
        $src=$this->db->query($query)->result();
        
        $tanda="";
        $tanda2="";
        $no=1;
        foreach ($saldowahasil as $s) {
            $duit='';
            $saldo+=$s->uang_nominal;
            if($s->uang_nominal <0){
                $duit="(".rupiah2($s->uang_nominal*-1).")";
                $tanda='min';
            }else{
                $duit=rupiah2($s->uang_nominal);
                $tanda="pos";
            }
            if($saldo<0){
                $tanda2='min';
            }else{
                $tanda2="pos";
            }
            $data[] = array (
				'uang_tgl' => date("d M Y", strtotime($dari_tanggal)),
				'uang_jns_transaksi' => $s->uang_jns_transaksi,
				'uang_nominal' => 0,
				'saldo' => rupiah2($saldo),
                'no'=>$no++,
                'tanda'=>$tanda,
                'tanda2'=>$tanda2,
			);
        }
        foreach ($src as $s) {
            $saldo+=$s->uang_nominal;
            $tgl=date("d M Y", strtotime($s->uang_tgl));
            $duit='';
            if($saldo<0){
                $tanda2='min';
            }else{
                $tanda2="pos";
            }
            if($s->uang_nominal <0){
                $duit="(".rupiah2($s->uang_nominal*-1).")";
                $tanda='min';
            }else{
                $duit=rupiah2($s->uang_nominal);
                $tanda="pos";
            }
            $data[] = array (
				'uang_tgl' => $tgl,
				'uang_jns_transaksi' => $s->uang_jns_transaksi,
				'uang_nominal' => $duit,
				'saldo' => rupiah2($saldo),
                'no'=>$no++,
                'tanda'=>$tanda,
                'tanda2'=>$tanda2,
			);
        }
       
        $response=array(
            'aaData'=>$data,
            'Saldo'=>array('saldoawal'=>0,'mutasi'=>0,'saldoakhir'=>0),
        );
        echo json_encode($response);
	}
	public function getData(){
        $akun_id=$this->input->post('akun_id');
        $dari_tanggal=$this->input->post('dari_tanggal');
        $hingga_tanggal=$this->input->post('hingga_tanggal');
        
        $query="";
    
        $query="select coalesce(sum(uang_nominal),0) as uang_nominal from keuangan where uang_akun_id='$akun_id' AND uang_tgl < '".$dari_tanggal."' and uang_jns_transaksi!='saldoawal'";
        $saldosebelum=$this->db->query($query)->row()->uang_nominal;

        $saldo=$saldosebelum;
        $query="SELECT '0.awal' as uang_id,swal_akun_id as uang_akun_id,swal_tgl_saldo_awal as uang_tgl,
                'Saldo Awal' as uang_jns_transaksi,swal_jumlah_saldo as uang_nominal
                FROM saldo_awal WHERE swal_akun_id='$akun_id'";
        $saldowahasil=$this->db->query($query)->result();
        foreach($saldowahasil as $sd){
            $saldo+=$sd->uang_nominal;
        }
        

        $query="SELECT coalesce(sum(uang_nominal),0) as totalmutasi FROM (
            SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
            FROM keuangan
            JOIN pembelian on(pmbl_id=uang_transaksi_id)
            where uang_jns_transaksi='pembelian' and pmbl_is_deleted='1' AND uang_akun_id='$akun_id'

            UNION

            SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
            FROM keuangan
            JOIN penjualan on(pnjl_id=uang_transaksi_id)
            where uang_jns_transaksi='penjualan' and pnjl_is_deleted='1' AND uang_akun_id='$akun_id'

            UNION
            
            SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
            FROM keuangan
            JOIN setoran on(stor_id=uang_transaksi_id)
            where uang_jns_transaksi='setoran'  and stor_is_deleted='1' AND uang_akun_id='$akun_id'

            UNION

            SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
            FROM keuangan
            JOIN penarikan on(trik_id=uang_transaksi_id)
            where uang_jns_transaksi='penarikan' and trik_is_deleted='1' AND uang_akun_id='$akun_id'
            UNION
            SELECT uang_id,uang_akun_id,uang_tgl,uang_jns_transaksi,uang_nominal 
            FROM keuangan
            JOIN deviden on(devi_id=uang_transaksi_id)
            where uang_jns_transaksi='deviden' and devi_is_deleted='1' AND uang_akun_id='$akun_id'
            ) as a
        WHERE a.uang_tgl 
        between '$dari_tanggal' and '$hingga_tanggal' ORDER BY uang_tgl asc";
        $saldomutasi=$this->db->query($query)->row()->totalmutasi;
        
        $saldoakhir=$saldomutasi+$saldo;
        if($saldomutasi<0){
            $tanda2="min";
        }else{
            $tanda2="pos";
        }
        if($saldo<0){
            $tanda='min';
        }else{
            $tanda="pos";
        }
        if($saldoakhir<0){
            $tanda3='min';
        }else{
            $tanda3="pos";
        }
        $data=array(
            'saldoawal'=>rupiah2($saldo),
            'saldomutasi'=>rupiah2($saldomutasi),
            'saldoakhir'=>rupiah2($saldoakhir),
            'tanda'=>$tanda,
            'tanda2'=>$tanda2,
            'tanda3'=>$tanda3,
        );
        echo json_encode($data);
    }
}
