<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gainloss extends MX_Controller {

	public function index()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'gain_loss_index',
		));
	}

    public function datatable($akun_id="",$saham="",$dari_tanggal,$hingga_tanggal)
	{
        
       
		$query="SELECT kshm_tgl_transaksi,seku_nama,akun_no_sid,kshm_jns_transaksi,
                kshm_jumlah,kshm_harga_rata_rata,kshm_gl,kshm_harga_netto,sham_kode,sham_nama
                FROM keuangan_saham 
                JOIN akun on kshm_akun_id=akun_id
                JOIN sekuritas on akun_seku_id=seku_id
                JOIN penjualan on kshm_transaksi_id=pnjl_id
                JOIN saham on kshm_sham_id=sham_id
                where kshm_tgl_transaksi<='$hingga_tanggal' 
                and kshm_tgl_transaksi>='$dari_tanggal' and kshm_jns_transaksi='penjualan'
                and pnjl_is_deleted='1'";
        if($akun_id!='all'){
            $query.=" and kshm_akun_id='$akun_id'";
        }
        if($saham!="" && $saham!="0" && !empty($saham)){
            $query.=" and kshm_sham_id='$saham'";
        };
        $query.=" ORDER by kshm_tgl_transaksi";
        $hasil=$this->db->query($query)->result();
        $totalgl=0;
        $no=1;
        $data=array();
        foreach($hasil as $h){
            $gl=$h->kshm_gl;
            $totalgl+=$h->kshm_gl;
            if($gl<0){
                $gltampil='('.rupiah2($gl*-1).')';
            }else{
                $gltampil=rupiah2($gl);
            }
            $data[]=array(
                'no'=>$no++,
                'tgl'=>$h->kshm_tgl_transaksi,
                'akun'=>$h->seku_nama. ' '.$h->akun_no_sid,
                'saham'=>$h->sham_kode.' ('.$h->sham_nama.')',
                'lot'=>angka($h->kshm_jumlah*-1),
                'shares'=>angka($h->kshm_jumlah*-100),
                'gl'=>$gltampil,
                'gl2'=>$h->kshm_gl,
                'harga_jual'=>rupiah2($h->kshm_harga_rata_rata),
            );
        }
        $response=array(
            'aaData'=>$data,
            'total'=>$totalgl,
        );
        echo json_encode($response);
	}

    public function getData(){
        $akun_id=$this->input->post('akun_id');
        $saham=$this->input->post('saham_id');
        $dari_tanggal=$this->input->post('dari_tanggal');
        $hingga_tanggal=$this->input->post('hingga_tanggal');
        $query="SELECT kshm_tgl_transaksi,seku_nama,akun_no_sid,kshm_jns_transaksi,
                kshm_jumlah,kshm_harga_rata_rata,kshm_gl,kshm_harga_netto,sham_kode,sham_nama
                FROM keuangan_saham 
                JOIN akun on kshm_akun_id=akun_id
                JOIN sekuritas on akun_seku_id=seku_id
                JOIN penjualan on kshm_transaksi_id=pnjl_id
                JOIN saham on kshm_sham_id=sham_id
                where kshm_tgl_transaksi<='$hingga_tanggal' 
                and kshm_tgl_transaksi>='$dari_tanggal' and kshm_jns_transaksi='penjualan'
                and pnjl_is_deleted='1'";
        if($akun_id!='all'){
            $query.=" and kshm_akun_id='$akun_id'";
        }
        if($saham!="" && $saham!="0"){
            $query.=" and kshm_sham_id='$saham'";
        };
        $query.=" ORDER by kshm_tgl_transaksi";
        $totalgl=0;
        $hasil=$this->db->query($query)->result();
        $no=1;
        $data=array();
        foreach($hasil as $h){
            $totalgl+=$h->kshm_gl;
        }
        
        if($totalgl<0){
            $gainloss='('.rupiah2($totalgl*-1).')';
            $tanda="min";

        }else{
            $gainloss=rupiah2($totalgl);
            $tanda="";
        }
        
        $hasil=array(
			'gainlos'=>$gainloss,
			'tanda'=>$tanda,
		);
        echo json_encode($hasil);
    }
}