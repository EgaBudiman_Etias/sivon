<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends MX_Controller {
    public function getGrupPengguna(){
        $id_vendor=$this->input->post('id_vendor');
        $src=$this->db->from('pengguna_grup')->where(array('id_vendor'=>$id_vendor,'is_deleted'=>'1'))->ORDER_BY('urutan','ASC')->get();
        header('Content-Type: application/json');
		echo json_encode($src->result());
    }
}   