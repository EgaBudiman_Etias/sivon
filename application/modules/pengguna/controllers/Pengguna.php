<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->form_validation->CI =& $this;
	}
	
	public function masuk()
	{
		$this->load->model('pengguna_grup_model');
		
		$nama = $this->input->post('nama');
		$kata_sandi = $this->input->post('kata_sandi');
		
		$hash_kata_sandi = hash_kata_sandi($kata_sandi);
		
		$src = $this->db
			->select('
				id,nama_pengguna,cookie,nama_lengkap,id_vendor,id_pengguna_grup
			')
			->from('pengguna')
			
			->where("'{$nama}' in (nama_pengguna)")
			->where('password', $hash_kata_sandi)
			->where('is_deleted', '1')
			
			->get();
			
		if ($src->num_rows() == 0) {
			$this->session->set_flashdata('status_masuk', 'gagal');
			redirect(site_url());
		}
		else {
			$pengguna = $src->row();
			
			$menu = $this->pengguna_grup_model->menu($pengguna->id);
			
			$this->session->set_userdata('pengguna', $pengguna);
			$this->session->set_userdata('menu', $menu);
			
			if ($this->input->post('ingat_saya')) {
				$isi_cookie = md5('sivon-cookie-'.date('YmdHis').'-'.mt_rand(100, 300));
				$hari_kadaluarsa = 30;
				
				$cookie = array (
					'name' => 'sivon_cookie',
					'value' => $isi_cookie,
					'expire' => $hari_kadaluarsa * 24 * 3600,
				);
				
				$this->input->set_cookie($cookie);
				$this->db->update('pengguna', array('cookie' => $isi_cookie), array('id' => $pengguna->id));
				
			}
			
			redirect(site_url('/dasboard'));
		}
	}
	
	public function profil()
	{
		$id = session_pengguna('id');
		// var_dump($id);
		if ($this->session->flashdata('data_form')) {
			$data = $this->session->flashdata('data_form');
		}
		else {
			$src = $this->db
				->from('pengguna')
				->where('is_deleted', '1')
				->where('id', $id)
				->get();
		
			if ($src->num_rows() == 0) {
				show_404();
			}
			else {
				$data = $src->row();
			}
		}
		
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_profil',
			'data' => $data,
		));
	}
	
	public function simpan_profil()
	{
		$validasi = array (
			array (
				'field' => 'nama_lengkap',
				'label' => 'Nama Pengguna',
				'rules' => 'required',
			),
			
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$kunci_data = array (
				'nama_lengkap',
				
			);
			
			$data = data_post($kunci_data);
			
			
			$data['updated_time'] = date('Y-m-d H:i:s');
			$data['updated_by'] = session_pengguna('id');
			
			$where = array('id' => session_pengguna('id'));
			
			$this->db->update('pengguna', $data, $where);
			
			$this->session->userdata('pengguna')->nama_lengkap = $data['nama_lengkap'];
			
			
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
			$this->session->set_flashdata('data_form', (object) $this->input->post());
		}
		
		redirect($this->agent->referrer());
	}
	
	
	
	public function ubah_kata_sandi()
	{
		$this->load->view('templates/site_tpl', array (
			'content' => 'pengguna_ubah_kata_sandi',
		));
	}
	
	public function cek_kata_sandi($kata_sandi)
	{
		$id = session_pengguna('id');
		$hash_kata_sandi = hash_kata_sandi($kata_sandi);
		
		$src = $this->db
			->from('pengguna')
			->where('is_deleted', '1')
			->where('id', $id)
			->where('password', $hash_kata_sandi)
			->get();
		
		if ($src->num_rows() == 0) {
			$this->form_validation->set_message('cek_kata_sandi', '<b>%s</b> salah.');
			return false;
		}
		else {
			return true;
		}
	}
	
	public function simpan_kata_sandi()
	{
		$validasi = array (
			array (
				'field' => 'kata_sandi_sekarang',
				'label' => 'Kata Sandi Sekarang',
				'rules' => 'required|callback_cek_kata_sandi',
			),
			array (
				'field' => 'kata_sandi_baru',
				'label' => 'Kata Sandi Baru',
				'rules' => 'required',
			),
		);
		
		$this->form_validation->set_rules($validasi);
		
		if ($this->form_validation->run()) {
			
			$id_pengguna = session_pengguna('id');
			$hash_kata_sandi_baru = hash_kata_sandi(trim($this->input->post('kata_sandi_baru')));
			
			$data = array (
				'password' => $hash_kata_sandi_baru,
				'updated_time' => date('Y-m-d H:i:s'),
				'updated_by' => $id_pengguna,
			);
			
			$where = array (
				'id' => $id_pengguna,
			);
			
			$this->db->update('pengguna', $data, $where);
			$this->session->set_flashdata('status_simpan', 'ok');
		}
		else {
			$this->session->set_flashdata('status_simpan', 'tidak_lengkap');
			$this->session->set_flashdata('validation_errors', validation_errors());
		}
		
		redirect(site_url('/pengguna/ubah-kata-sandi'));
	}
	
	public function keluar()
	{
		$id_pengguna = session_pengguna('id');
		$data = array (
			'cookie' =>null,
		);
		
		$where = array (
			'id' => $id_pengguna,
		);
			
		$this->db->update('pengguna', $data, $where);
		$this->session->unset_userdata('pengguna');
		$this->session->unset_userdata('menu');
		$this->session->sess_destroy();
		
		delete_cookie('saham_cookie');
		
		redirect(site_url());
	}
	
}
