<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_grup_model extends CI_Model {

	public function menu($grup_peng_id)
	{
		$sql = "
			select b.*
			from (
				select id_menu
				from pengguna_grup_menu
				where
					id_pengguna_grup = ?
					and is_deleted = '1'
			) as a
			join menu as b on
				a.id_menu = b.id
				and b.is_deleted = '1'
			order by nomor
			
		";
		$src = $this->db->query($sql, array($grup_peng_id));
		
		$menu = array();
		
		foreach ($src->result() as $row) {
			$val = array (
				'kode' => $row->kode,
				'ikon' => $row->ikon,
				'teks' => $row->teks,
				'uri' => $row->uri,
			);
			
			if ($row->id_induk == null) {
				$menu[$row->id] = $val;
			}
			else {
				$menu[$row->id_induk]['sub'][] = $val;
			}
		}
		
		return $menu;
	}
	
}
