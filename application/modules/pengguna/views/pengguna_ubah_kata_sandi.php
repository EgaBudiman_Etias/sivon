<div class="col-sm-4 offset-sm-4 p-0">
	<div class="card">
		<div class="card-header">Ubah Kata Sandi</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Kata Sandi berhasil diubah.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo site_url('/pengguna/simpan-kata-sandi'); ?>">
				<div class="form-group">
					<label>Kata Sandi Sekarang</label>
					<input type="text" class="form-control" name="kata_sandi_sekarang">
				</div>
				<div class="form-group">
					<label>Kata Sandi Baru</label>
					<input type="text" class="form-control" name="kata_sandi_baru">
				</div>
				<button type="submit" class="btn btn-primary">Ubah Kata Sandi</button>
			</form>
		</div>
	</div>
</div>