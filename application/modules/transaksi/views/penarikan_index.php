<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Transaksi Penarikan
			<a href="penarikan/tambah" class="btn btn-primary btn-sm btn-header" style="margin-left:5px;">
				<i class="ti ti-write"></i> Tambah Data
			</a>
			<button class="btn btn-sm btn-info btn-header" data-toggle="modal" data-target="#exampleModal" >
				<i class="ti ti-filter"></i> Filter 
			</button>
			&nbsp;&nbsp;
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Kode Penarikan</th>
						<th>Tanggal</th>
						<th>Nama Sekuritas</th>
						<th>Nama Bank</th>
                        <th>No Rekening RDN</th>
						<th>Nama Bank RDN</th>
						<th>No SID</th>
                        <th>Nominal</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kriteria Filter Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  	<form id="form-filter">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Sekuritas
				</label>
				<div class="col-sm-6 pr-sm-0">
					<select name="seku_id" data-width="100%" id="seku_id" class="form-control select2">
						<option value="">--Pilih Sekuritas--</option>
						<?=options_sekuritas()?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Bank
				</label>
				<div class="col-sm-6 pr-sm-0">
					<select name="bank_id" data-width="100%" id="bank_id" class="form-control select2">
						<option value="">--Pilih Bank--</option>
						<?=options_bank()?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Tanggal
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control" name="tgl" id="tgl" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> <strong>Total Penarikan </strong>
				</label>
				
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Minimal
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control duit" name="dari_pembelian" id="dari_pembelian" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Maximal
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control  duit" name="hingga_pembelian" id="hingga_pembelian" value="">
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" onclick="resetFilter()">Reset Filter</button>
        <button type="button" class="btn btn-primary" onclick="init_datatable()">Filter</button>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
	function resetFilter(){
		$('#seku_id').val('').trigger('change');
		$('#bank_id').val('').trigger('change');
		$('#exampleModal')
			.find("input")
			.val('')
			.end();
		init_datatable();
	}
function init_datatable()
{
	var bank_id=$("#bank_id").val();
	var seku_id=$('#seku_id').val();
	var tgl=$('#tgl').val();
	var dari_pembelian=$('#dari_pembelian').val();
	var hingga_pembelian=$('#hingga_pembelian').val();
	datatable = $('#datatable').DataTable ({
		'destroy': true,
		'bInfo': true,
		'serverSide': true,
		'serverMethod': 'post',
		'ajax': {
			'type': 'POST',
			'url':'<?php echo site_url('/transaksi/penarikan/datatable'); ?>',
			'data':{bank_id:bank_id,seku_id:seku_id,tgl:tgl,dari_pembelian:dari_pembelian,hingga_pembelian:hingga_pembelian},
		},
		'order': [[ 3, 'desc' ]],
		'fixedHeader': true,
		'columns': [
			{
				data: function (row, type, val, meta) {
                    return '' +
                        '<a class="btn btn-action btn-primary" href="penarikan/ubah/'+row.trik_id+'">'+
                            '<i class="ti ti-pencil-alt"></i>'+
                        '</a>&nbsp;'+
						'<a class="btn btn-action btn-danger btn-delete" onclick="return checkDelete()" href="penarikan/delete/'+row.trik_id+'">'+
                            '<i class="ti ti-trash"></i>'+
                        '</a>';
                },
				orderable: false,
				className: 'dt-body-center'
			},
            { data: 'no', orderable: false },
			{ data: 'trik_kode'},
			{ data: 'trik_tgl'},
			{ data: 'seku_nama' },
			{ data: 'bank_nama' },
			{ data: 'rek_tujuan' },
			{ data: 'bank_tujuan'},
			{ data: 'akun_no_sid'},
            { data: 'trik_nominal',className: 'dt-body-right' },
		]
	});
	$('#exampleModal').modal('hide');
}

$().ready(function() {
	
	init_datatable();
	$('#exampleModal').on('shown.bs.modal', function() {
		$('#tgl').Zebra_DatePicker({
			
			default_position: 'below'
		});
		$('.duit').number(true, 0);
	});
});
</script>