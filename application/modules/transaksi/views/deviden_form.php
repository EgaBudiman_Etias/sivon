<script type="text/javascript" src="<?=base_url()?>assets/js/deviden.js"></script>
<div class="col-12 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Deviden
			<a href="<?php echo site_url('transaksi/deviden'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" class="row dk-form dk-form-confirm">
				<input type="hidden" name="devi_id" value="<?php if ($data != null) echo $data->devi_id; ?>">
				<div class="col-sm-4 col-md-3">
					<br>
					<div class="form-group">
						<label>Kode Deviden</label>
						<input type="text" class="form-control" name="devi_kode" value="<?php if ($data != null){ echo $data->devi_kode;} else{echo kode_deviden();}; ?>" readonly>
						<input type="hidden" class="form-control" id="pajak_deviden" name="pajak_persen" value="<?=$pajak?>" readonly>
						
					</div>
					<div class="form-group">
						<label>Tgl. Deviden</label>
						<input type="text" class="form-control tanggalan" name="devi_tgl_bagi" value="<?php if ($data != null) echo $data->devi_tgl_bagi; else echo date('Y-m-d'); ?>" readonly>
					</div>
					<div class="form-group">
						<label>Akun</label>
						<select class="form-control select2" name="devi_akun_id2" id="devi_akun_id" data-placeholder="Pilih Akun" style="width:100%" <?php if ($aksi == 'ubah') echo 'disabled'; ?>>
							<option value=""></option>
							<?=options_akun2($data!=null?$data->devi_akun_id:"")?>
						</select>
						
						<input type="hidden" id="devi_akun_id2" name="devi_akun_id" value="<?php echo $data!=null?$data->devi_akun_id:""; ?>">
						
					</div>
					
				</div>
				<div class="col-sm-8 col-md-9">
					<div class="mb-3">
						<button class="btn btn-sm btn-primary float-right" type="submit">
							<i class="ti ti-save"></i> Simpan
						</button>
						<div class="clearfix"></div>
					</div>
					<table class="table-cell dk-deviden-table" width="100%">
						<thead>
							<tr>
								<th>Saham</th>
								<th width="70px">Jumlah Lot</th>
                                <th width="80px">Jumlah Shares</th>
								<th width="120px">Deviden / Shares (Rp)</th>
								<th width="120px">Bruto (Rp)</th>
								<th width="120px">Pajak Deviden (Rp)</th>
                                <th width="120px">Netto (Rp)</th>
								<th width="30px">Hapus</th>
							</tr>
						</thead>
						<tbody>
							<?php $saham_null = array(); ?>
							<?php $saham_null['ddev_sham_id'][] = ''; ?>
							<?php $saham_null['ddev_jumlah_lot'][] = ''; ?>
							<?php $saham_null['ddev_jumlah_shares'][] = ''; ?>
							<?php $saham_null['ddev_deviden_shares'][] = ''; ?>
							<?php $saham_null['ddev_bruto'][] = ''; ?>
							<?php $saham_null['ddev_pajak_deviden'][] = ''; ?>
							<?php $saham_null['ddev_pajak_deviden_persen'][] = ''; ?>
							<?php $saham_null['ddev_netto'][] = ''; ?>
							
							
							<?php $p = $data == null ? $saham_null : $data->saham; ?>
							
							<?php foreach ($p['ddev_sham_id'] as $i => $sham_id): ?>
							<tr>
								<td>
									<select class="form-control select2 dk-deviden-saham" name="saham[ddev_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">
										<option value=""></option>
                                        <?=options_saham($p['ddev_sham_id'][$i])?>
									</select>
								</td>
								<td><input type="text" name="saham[ddev_jumlah_lot][]" class="control-number-no dk-deviden-jumlah-lot" style="width:70px" value="<?php echo $p['ddev_jumlah_lot'][$i]; ?>" ></td>
								<td><input type="text" name="saham[ddev_jumlah_shares][]" class="control-number-no dk-deviden-jumlah-shares" style="width:80px" value="<?php echo $p['ddev_jumlah_shares'][$i]; ?>" readonly></td>
								<td><input type="text" name="saham[ddev_deviden_shares][]" class="control-number-digit dk-deviden-deviden-shares" style="width:120px" value="<?php echo $p['ddev_deviden_shares'][$i]; ?>"></td>
								<td><input type="text" name="saham[ddev_bruto][]" class="control-number-digit dk-deviden-bruto" style="width:120px" value="<?php echo $p['ddev_bruto'][$i]; ?>" readonly></td>
								<td>
									
									<input type="hidden" name="saham[ddev_pajak_deviden_persen][]"  class="control-number-digit dk-deviden-pajak" style="width:120px" value="<?php echo $p['ddev_pajak_deviden_persen'][$i]; ?>">
									<input type="text" name="saham[ddev_pajak_deviden][]"  class="control-number-digit dk-deviden-pajak-rp" readonly style="width:120px" value="<?php echo $p['ddev_pajak_deviden'][$i]; ?>">
								</td>
								<td><input type="text" class="control-number-digit dk-deviden-netto" name="saham[ddev_netto][]" style="width:120px" value="<?php echo $p['ddev_netto'][$i]; ?>" readonly></td>
                                
								<td align="center">
									<button class="btn btn-action btn-danger dk-deviden-btn-remove">
										<i class="ti ti-trash"></i>
									</button>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
									<!-- <button class="btn btn-info dk-deviden-btn-add">
										<i class="ti ti-plus"></i>
										Tambah Saham
									</button> -->
								</td>
								<td align="right"><strong>TOTAL</strong></td>
								
								<td align="right"><input type="text" value="<?php echo $data!=null?$data->devi_grand_tot:""?>" name="devi_grand_tot" class="control-number-digit dk-deviden-total-harga" style="width:120px" readonly></td>
								<td>&nbsp;</td>
							</tr>
							
						</tfoot>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
