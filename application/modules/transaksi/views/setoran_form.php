<script type="text/javascript" src="<?=base_url()?>assets/js/setoran.js"></script>
<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Transaksi Setoran
			<a href="<?php echo site_url('/transaksi/setoran'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="stor_id" value="<?php if ($data != null) echo $data->stor_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kode Setoran
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" readonly class="form-control" name="stor_kode" value="<?php if ($data != null) {echo $data->stor_kode;}else{echo kode_setoran();} ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Tanggal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" readonly class="form-control tanggalan" name="stor_tgl" value="<?php if ($data != null) {echo $data->stor_tgl;}else{echo date('Y-m-d');} ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Akun 
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="stor_akun_id" onchange="getData()" id="stor_akun_id" class="form-control akun">
                            <option value="">--Pilih Akun--</option>
                            <?=options_akun($data!=null?$data->stor_akun_id:"")?>
                        </select>
					</div>
				</div>
				<input type="hidden" value="<?=$data!=null?$data->stor_akun_id:''?>" id="stor_akun_id2">
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Nominal Saldo
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" readonly id="saldo_akun"  class="form-control control-number-digit" value="0">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> BANK ASAL
					</label>
					<div class="col-sm-6 pr-sm-0">
                    <select name="stor_bank_id" class="form-control select2">
                            <option value="">--Pilih Bank--</option>
                            <?=options_bank($data!=null?$data->stor_bank_id:"")?>
                        </select>
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> No Referensi
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="stor_no_ref" value="<?php if ($data != null) echo $data->stor_no_ref; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nominal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control control-number-digit" name="stor_nominal" value="<?php if ($data != null) {echo $data->stor_nominal;} else{echo '0';} ?>">
						<input type="hidden" class="form-control control-number-no" name="stor_nominal_awal" value="<?php if ($data != null) {echo $data->stor_nominal; } else{echo '0';}?>">
					</div>
				</div>
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>