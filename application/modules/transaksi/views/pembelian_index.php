<div class="col-md-12 offset-md-0 p-0">
	<div class="card">
		<div class="card-header">
			Transaksi Pembelian
			<a href="pembelian/tambah" class="btn btn-primary btn-sm btn-header" style="margin-left:5px;">
				<i class="ti ti-write"></i> Tambah Data
			</a>
			<button class="btn btn-sm btn-info btn-header" data-toggle="modal" data-target="#exampleModal" >
				<i class="ti ti-filter"></i> Filter 
			</button>
		</div>
		<div class="card-body">
			<table class="cell-border stripe order-column hover" id="datatable">
				<thead>	
					<tr>
						<th width="50px">Aksi</th>
						<th width="10px">No.</th>
						<th>Kode Pembelian</th>
						<th>Tanggal</th>
						<th>Kode Akun</th>
						<th>Nama Sekuritas</th>
                        <th>Nomor SID</th>
						<th>Nomor KSEI/KPEI</th>
                        <th>Total Pembelian</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kriteria Filter Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  	<form id="form-filter">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Akun
				</label>
				<div class="col-sm-6 pr-sm-0">
					<select name="seku_id" data-width="100%" id="akun_id" class="form-control akun">
						<option value="">--Pilih Akun--</option>
						<?=options_akun()?>
					</select>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Tanggal
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control" name="tgl" id="tgl" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> No SID
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control" name="no_sid" id="no_sid" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> No KSEI KPEI
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control" name="no_ksei" id="no_ksei" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> <strong>Total Pembelian </strong>
				</label>
				
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Minimal
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control duit" name="dari_pembelian" id="dari_pembelian" value="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
					<span class="text-danger"></span> Maximal
				</label>
				<div class="col-sm-6 pr-sm-0">
					<input type="text" class="form-control  duit" name="hingga_pembelian" id="hingga_pembelian" value="">
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
	  	<button type="button" class="btn btn-warning" onclick="resetFilter()">Reset Filter</button>
        <button type="button" class="btn btn-primary" onclick="init_datatable()">Filter</button>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function checkDelete(){
        return confirm('Yakin Untuk Menghapus?');
    }
	function init_datatable()
	{
		var akun_id=$('#akun_id').val();
		var tgl=$('#tgl').val();
		var no_sid=$('#no_sid').val();
		var no_ksei=$('#no_ksei').val();
		var dari_pembelian=$('#dari_pembelian').val();
		var hingga_pembelian=$('#hingga_pembelian').val();
		datatable = $('#datatable').DataTable ({
			'destroy': true,
			'bInfo': true,
			'serverSide': true,
			'serverMethod': 'post',
			'ajax': {
				'type': 'POST',
				'url':'<?php echo site_url('/transaksi/pembelian/datatable'); ?>',
				'data':{akun_id:akun_id,tgl:tgl,no_sid:no_sid,no_ksei:no_ksei,dari_pembelian:dari_pembelian,hingga_pembelian:hingga_pembelian},
			},

			'order': [[ 3, 'desc' ]],
			'fixedHeader': true,
			'columns': [
				{
					data: function (row, type, val, meta) {
						return '' +
							'<a class="btn btn-action btn-primary" href="pembelian/ubah/'+row.pmbl_id+'">'+
								'<i class="ti ti-pencil-alt"></i>'+
							'</a>&nbsp;'+
							'<a class="btn btn-action btn-danger btn-delete" href="pembelian/delete/'+row.pmbl_id+'">'+
								'<i class="ti ti-trash"></i>'+
							'</a>';
					},
					orderable: false,
					className: 'dt-body-center'
				},
				{ data: 'no', orderable: false },
				{ data: 'pmbl_kode'},
				{ data: 'pmbl_tgl_beli'},
				{ data: 'akun_kode'},
				
				{ data: 'seku_nama' },
				{ data: 'akun_no_sid' },
				{ data: 'akun_no_ksei_kpei'},
				{ data: 'pmbl_grand_tot',className: 'dt-body-right'},
				
			]
		});
		$('#exampleModal').modal('hide');
	}

	function resetFilter(){
		
		$('#akun_id').val('').trigger('change');
		// $('#bank_id').val('').trigger('change');
		$('#exampleModal')
			.find("input")
			.val('')
			.end();
		init_datatable();
	}
	$().ready(function() {
	
		init_datatable();
		$('#exampleModal').on('shown.bs.modal', function() {
			$('#tgl').Zebra_DatePicker({
				
				default_position: 'below'
			});
			$('.duit').number(true, 0);
		});
	});
</script>