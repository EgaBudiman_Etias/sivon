<script type="text/javascript" src="<?=base_url()?>assets/js/penjualan.js"></script>
<div class="col-12 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Penjualan
			<a href="<?php echo site_url('transaksi/penjualan'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" class="row dk-form dk-form-confirm">
				<input type="hidden" name="pnjl_id" value="<?php if ($data != null) echo $data->pnjl_id; ?>">
				<div class="col-sm-4 col-md-3">
					<br>
					<div class="form-group">
						<label>Kode Penjualan</label>
						<input type="text" class="form-control " name="pnjl_kode" value="<?php if ($data != null) echo $data->pnjl_kode; else echo kode_penjualan(); ?>" readonly>
					</div>
					<div class="form-group">
						<label>Tgl. Penjualan</label>
						<input type="text" class="form-control tanggalan" name="pnjl_tgl_jual" value="<?php if ($data != null) echo $data->pnjl_tgl_jual; else echo date('Y-m-d'); ?>" readonly>
					</div>
					<div class="form-group">
						<label>Akun</label>
						<select class="form-control select2" name="pnjl_akun_id2" id="pnjl_akun_id"  style="width:100%" <?php if ($aksi == 'ubah') echo 'disabled'; ?>>
							<option value="0">--Pilih Akun--</option>
							<?=options_akun2($data!=null?$data->pnjl_akun_id:"")?>
						</select>
					
						<input type="hidden" id="pnjl_akun_id2" name="pnjl_akun_id" value="<?php echo $data!=null?$data->pnjl_akun_id:""; ?>">
						
					</div>
					<div class="form-group">
						<label>Saldo RDN</label>
						<input type="text" class="form-control control-number-digit"  value="0" name="saldo_rdn" id="saldo_rdn" style="text-align: right; " readonly>
						<input type="hidden" class="form-control"  value="<?=$pajak?>" name="pajak" id="pajak" style="text-align: right; " readonly>
						<input type="hidden" class="form-control"  value="<?=$aksi?>" name="aksi" id="aksi" style="text-align: right; " readonly>
					</div>
				</div>
				<div class="col-sm-12 col-md-12">
					<div class="mb-3">
						<button class="btn btn-sm btn-primary float-right" type="submit">
							<i class="ti ti-save"></i> Simpan
						</button>
						<div class="clearfix"></div>
					</div>
					<table class="table-cell dk-jual-table" width="100%">
						<thead>
							<tr>
								<th width="180px">Saham</th>
								<th width="100px">Juml Lot Saat Ini</th>
								<th width="70px">Juml Lot Jual</th>
                                <th width="80px">Juml Shares</th>
								<th width="120px">Harga / Shares (Rp)</th>
								<th width="120px">Bruto (Rp)</th>
								<th width="120px">Fee Penjualan (Rp)</th>
								<th width="120px">Pajak Penjualan (Rp)</th>
                                <th width="120px">Netto (Rp)</th>
								<th width="30px">Hapus</th>
							</tr>
						</thead>
						<tbody>
							<?php $saham_null = array(); ?>
							<?php $saham_null['dpjl_sham_id'][] = ''; ?>
							<?php $saham_null['dpjl_jumlah_lot'][] = ''; ?>
							<?php $saham_null['dpjl_jumlah_shares'][] = ''; ?>
							<?php $saham_null['dpjl_harga_shares'][] = ''; ?>
							<?php $saham_null['dpjl_bruto'][] = ''; ?>
							<?php $saham_null['dpjl_fee_penjualan'][] = ''; ?>
							<?php $saham_null['dpjl_fee_penjualan_persen'][] = ''; ?>
							<?php $saham_null['dpjl_pajak_penjualan'][] = ''; ?>
							<?php $saham_null['dpjl_netto'][] = ''; ?>
							<?php $saham_null['dpjl_jumlah_lot_saat_ini'][] = ''; ?>
							
							<?php $p = $data == null ? $saham_null : $data->saham; ?>
							
							<?php foreach ($p['dpjl_sham_id'] as $i => $sham_id): ?>
							<tr>
								<td>
									<select class="form-control select2 dk-jual-saham" name="saham[dpjl_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">
										<option value=""></option>
                                        <?=options_saham_jual($p['dpjl_sham_id'][$i])?>
									</select>
								</td>
								<td><input type="text" name="saham[dpjl_jumlah_lot_saat_ini][]" class="control-number-no dk-jual-jumlah-lot-saat-ini" value="<?php echo $p['dpjl_jumlah_lot_saat_ini'][$i]; ?>"  style="width:100%" readonly ></td>
								<td><input type="text" name="saham[dpjl_jumlah_lot][]" class="control-number-no dk-jual-jumlah-lot" style="width:100%" value="<?php echo $p['dpjl_jumlah_lot'][$i]; ?>" ></td>
								<td><input type="text" name="saham[dpjl_jumlah_shares][]" class="control-number-no dk-jual-jumlah-shares" style="width:100%" value="<?php echo $p['dpjl_jumlah_shares'][$i]; ?>" readonly></td>
								<td><input type="text" name="saham[dpjl_harga_shares][]" class="control-number-digit dk-jual-harga-shares" style="width:100%" value="<?php echo $p['dpjl_harga_shares'][$i]; ?>"></td>
								<td><input type="text" name="saham[dpjl_bruto][]" class="control-number-digit dk-jual-bruto" style="width:100%" value="<?php echo $p['dpjl_bruto'][$i]; ?>" readonly></td>
								<td>
									
									<input type="hidden" name="saham[dpjl_fee_penjualan_persen][]"  class="control-number-no dk-jual-fee-penjualan" style="width:100%" value="<?php echo $p['dpjl_fee_penjualan_persen'][$i]; ?>">
									<input type="text" name="saham[dpjl_fee_penjualan][]"  class="control-number-digit dk-jual-fee-penjualan-rp" readonly style="width:100%" value="<?php echo $p['dpjl_fee_penjualan'][$i]; ?>">
								</td>
								<td><input type="text" name="saham[dpjl_pajak_penjualan][]" class="control-number-digit dk-jual-pajak" style="width:100%" value="<?php echo $p['dpjl_pajak_penjualan'][$i]; ?>" readonly></td>
								<td><input type="text" class="control-number-digit dk-jual-netto" name="saham[dpjl_netto][]" style="width:100%" value="<?php echo $p['dpjl_netto'][$i]; ?>" readonly></td>
                                
								<td align="center">
									<button class="btn btn-action btn-danger dk-jual-btn-remove">
										<i class="ti ti-trash"></i>
									</button>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7">
									<!-- <button class="btn btn-info dk-jual-btn-add">
										<i class="ti ti-plus"></i>
										Tambah Saham
									</button> -->
								</td>
								<td align="right"><strong>TOTAL</strong></td>
								
								<td align="right"><input type="text" value="<?php echo $data!=null?$data->pnjl_grand_tot:""?>" name="pnjl_grand_tot" class="control-number-digit dk-jual-total-harga" style="width:100%" readonly></td>
								<td>&nbsp;</td>
							</tr>
							
						</tfoot>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
