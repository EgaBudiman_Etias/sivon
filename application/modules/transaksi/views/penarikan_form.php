<script type="text/javascript" src="<?=base_url()?>assets/js/penarikan.js"></script>
<div class="col-md-6 offset-md-3 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Transaksi Penarikan
			<a href="<?php echo site_url('/transaksi/penarikan'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>">
				<input type="hidden" name="trik_id" value="<?php if ($data != null) echo $data->trik_id; ?>">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Kode Penarikan
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" readonly class="form-control " name="trik_kode" value="<?php if ($data != null) {echo $data->trik_kode;}else{echo kode_penarikan();} ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Tanggal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" readonly class="form-control tanggalan" name="trik_tgl" value="<?php if ($data != null) {echo $data->trik_tgl;}else{echo date('Y-m-d');} ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Akun 
					</label>
					<div class="col-sm-6 pr-sm-0">
						<select name="trik_akun_id" id="trik_akun_id" onchange="getData()" class="form-control akun">
                            <option value="">--Pilih Akun--</option>
                            <?=options_akun($data!=null?$data->trik_akun_id:"")?>
                        </select>
					</div>
				</div>
				<input type="hidden" value="<?=$data!=null?$data->trik_akun_id:''?>" id="trik_akun_id2">
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> Nominal Saldo
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" readonly id="saldo_akun" name="saldo_akun" class="form-control control-number-digit" value="0">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> BANK TUJUAN
					</label>
					<div class="col-sm-6 pr-sm-0">
                    <select name="trik_bank_id" class="form-control select2">
                            <option value="">--Pilih Bank--</option>
                            <?=options_bank($data!=null?$data->trik_bank_id:"")?>
                        </select>
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger"></span> No Referensi
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control" name="trik_no_ref" value="<?php if ($data != null) echo $data->trik_no_ref; ?>">
					</div>
				</div>
                <div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">
						<span class="text-danger">*</span> Nominal
					</label>
					<div class="col-sm-6 pr-sm-0">
						<input type="text" class="form-control control-number-digit" name="trik_nominal" value="<?php if ($data != null) {echo $data->trik_nominal;}else{echo "0";} ?>">
						<input type="hidden" class="form-control control-number-digit" name="trik_nominal_awal" value="<?php if ($data != null) {echo $data->trik_nominal;} else{echo "0";}?>">
					</div>
				</div>
                
				<div class="form-group row">
					<label class="col-sm-4 col-form-label pr-sm-0 text-sm-right">&nbsp;</label>
					<div class="col-sm-6 pr-sm-0">
						<button type="submit" class="btn btn-primary">Simpan Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>