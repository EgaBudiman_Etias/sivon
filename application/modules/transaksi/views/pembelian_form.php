<script type="text/javascript" src="<?=base_url()?>assets/js/pembelian.js"></script>
<div class="col-12 p-0 mb-4">
	<div class="card">
		<div class="card-header">
			Form Pembelian
			<a href="<?php echo site_url('transaksi/pembelian'); ?>" class="btn btn-outline-primary btn-sm btn-header">
				<i class="ti ti-back-left"></i> Kembali
			</a>
		</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('status_simpan') == 'ok'): ?>
			<div class="alert alert-success">Data berhasil disimpan.</div>
			<?php endif; ?>
			
			<?php if ($this->session->flashdata('status_simpan') == 'tidak_lengkap'): ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('validation_errors'); ?></div>
			<?php endif; ?>
			
			<form method="post" action="<?php echo $url_aksi; ?>" class="row dk-form dk-form-confirm">
				<input type="hidden" name="pmbl_id" value="<?php if ($data != null) echo $data->pmbl_id; ?>">
				<div class="col-sm-4 col-md-3">
					<br>
					<div class="form-group">
						<label>Kode Pembelian</label>
						<input type="text" class="form-control" name="pmbl_kode" value="<?php if ($data != null) echo $data->pmbl_kode; else echo kode_pembelian(); ?>" readonly>
					</div>
					<div class="form-group">
						<label>Tgl. Pembelian</label>
						<input type="text" class="form-control tanggalan" name="pmbl_tgl_beli" value="<?php if ($data != null) echo $data->pmbl_tgl_beli; else echo date('Y-m-d'); ?>" readonly>
					</div>
					<div class="form-group">
						<label>Akun</label>
						<select class="form-control select2" name="pmbl_akun_id2" id="pmbl_akun_id" style="width:100%" <?php if ($aksi == 'ubah') echo 'disabled'; ?>>
							<option value="0">--Pilih Akun--</option>
							<?=options_akun2($data!=null?$data->pmbl_akun_id:"")?>
						</select>
						
						<input type="hidden" id="pmbl_akun_id2" name="pmbl_akun_id" value="<?php echo $data!=null?$data->pmbl_akun_id:""; ?>">
						
					</div>
					<div class="form-group">
						<label>Saldo RDN</label>
						<input type="text" class="form-control control-number-digit" value='0' name="saldo_rdn" id="saldo_rdn" style="text-align: right; " readonly>
					</div>
				</div>
				<div class="col-sm-12 col-md-12">
					<div class="mb-3">
						<button class="btn btn-sm btn-primary float-right" type="submit">
							<i class="ti ti-save"></i> Simpan
						</button>
						<div class="clearfix"></div>
					</div>
					<table class="table-cell dk-beli-table" width="100%">
						<thead>
							<tr>
								<th width="180px">Saham</th>
								<th width="100px">Juml Lot Saat Ini</th>
								<th width="70px">Juml Lot Beli</th>
                                <th width="80px">Juml Shares</th>
								<th width="120px">Harga / Shares (Rp)</th>
								<th width="120px">Bruto (Rp)</th>
								<th width="120px">Fee Pembelian (Rp)</th>
                                <th width="120px">Netto (Rp)</th>
								<th width="30px">Hapus</th>
							</tr>
						</thead>
						<tbody>
							<?php $saham_null = array(); ?>
							<?php $saham_null['dbli_sham_id'][] = ''; ?>
							<?php $saham_null['dbli_jumlah_lot_saat_ini'][] = ''; ?>
							<?php $saham_null['dbli_jumlah_lot'][] = ''; ?>
							<?php $saham_null['dbli_jumlah_shares'][] = ''; ?>
							<?php $saham_null['dbli_harga_shares'][] = ''; ?>
							<?php $saham_null['dbli_bruto'][] = ''; ?>
							<?php $saham_null['dbli_fee_pembelian'][] = ''; ?>
							<?php $saham_null['dbli_fee_pembelian_persen'][] = ''; ?>
							<?php $saham_null['dbli_netto'][] = ''; ?>
							
							
							<?php $p = $data == null ? $saham_null : $data->saham; ?>
							
							<?php foreach ($p['dbli_sham_id'] as $i => $sham_id): ?>
							<tr>
								<td>
									<select class="form-control select2 dk-beli-saham" name="saham[dbli_sham_id][]" data-placeholder="Pilih Saham" style="width:100%">
										<option value=""></option>
                                        <?=options_saham($p['dbli_sham_id'][$i])?>
									</select>
								</td>
								<td><input type="text" name="saham[dbli_jumlah_lot_saat_ini][]" class="control-number-no dk-beli-jumlah-lot-saat-ini" value="<?php echo $p['dbli_jumlah_lot_saat_ini'][$i]; ?>"  style="width:100% ;text-align: right; " readonly ></td>
								<td><input type="text" name="saham[dbli_jumlah_lot][]" class="control-number-no dk-beli-jumlah-lot" style="width:100%;text-align: right; " value="<?php echo $p['dbli_jumlah_lot'][$i]; ?>" ></td>
								<td><input type="text" name="saham[dbli_jumlah_shares][]" class="control-number-no dk-beli-jumlah-shares" style="width:100%;text-align: right; " value="<?php echo $p['dbli_jumlah_shares'][$i]; ?>" readonly></td>
								<td><input type="text" name="saham[dbli_harga_shares][]" class="control-number-digit dk-beli-harga-shares" style="width:100%;text-align: right; " value="<?php echo $p['dbli_harga_shares'][$i]; ?>"></td>
								<td><input type="text" name="saham[dbli_bruto][]" class="control-number-digit dk-beli-bruto" style="width:100%;text-align: right; " value="<?php echo $p['dbli_bruto'][$i]; ?>" readonly></td>
								<td>
									
									<input type="hidden" name="saham[dbli_fee_pembelian_persen][]"  class="control-number-digit dk-beli-fee-pembelian" style="width:100%;text-align: right; " value="<?php echo $p['dbli_fee_pembelian_persen'][$i]; ?>">
									<input type="text" name="saham[dbli_fee_pembelian][]"  class="control-number-digit dk-beli-fee-pembelian-rp" readonly style="width:100%;text-align: right; " value="<?php echo $p['dbli_fee_pembelian'][$i]; ?>">
								</td>
								<td><input type="text" class="control-number-digit dk-beli-netto" name="saham[dbli_netto][]" style="width:100%;text-align: right; " value="<?php echo $p['dbli_netto'][$i]; ?>" readonly></td>
                                
								<td align="center">
									<button class="btn btn-action btn-danger dk-beli-btn-remove">
										<i class="ti ti-trash"></i>
									</button>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<button class="btn btn-info dk-beli-btn-add">
										<i class="ti ti-plus"></i>
										Tambah Saham
									</button>
								</td>
								<td align="right"><strong>TOTAL</strong></td>
								
								<td align="right"><input type="text" value="<?php echo $data!=null?$data->pmbl_grand_tot:""?>" name="pmbl_grand_tot" class="control-number-digit dk-beli-total-harga" style="width:100%;text-align: right; " readonly></td>
								<td>&nbsp;</td>
							</tr>
							<!-- <tr>
								<td colspan="5" align="right">Diskon (Rp)</td>
								<td>&nbsp;</td>
								<td align="right"><input type="text" name="diskon" class="control-number dk-order-diskon" value="<?php if ($data != null) echo $data->diskon; ?>" style="width:120px"></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="5" align="right">PPN (Rp)</td>
								<td align="center"><input type="checkbox" name="is_ppn" value="1" class="dk-order-is-ppn" <?php if ($data != null) if ($data->is_ppn == 1) echo 'checked'; ?>></td>
								<td align="right"><input type="text" name="ppn" class="control-number dk-order-ppn" style="width:120px" readonly></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="5" align="right">Biaya Lain-lain (Rp)</td>
								<td>&nbsp;</td>
								<td align="right"><input type="text" name="biaya_lain" class="control-number dk-order-biaya-lain" value="<?php if ($data != null) echo $data->biaya_lain; ?>" style="width:120px"></td>
								<td>&nbsp;</td>
							</tr> -->
							<!-- <tr>
								<td colspan="6" align="right"><b>GRAND TOTAL (Rp)</b></td>
								
								<td align="right"><input type="text" class="control-number dk-order-grand-total" style="width:120px" readonly></td>
								<td>&nbsp;</td>
							</tr> -->
						</tfoot>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
