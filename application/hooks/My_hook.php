<?php

function cek_session()
{
	$ci =& get_instance();
	
	$ci->load->model('pengguna/pengguna_grup_model');

	if ( ! session_pengguna('id')) {
		$cookie = $ci->input->cookie('sivon_cookie');
		
		$src = $ci->db
			->select('
				*
			')
			->from('pengguna')
			->where('cookie', $cookie)
			->where('cookie is not null')
			->where('is_deleted', 1)
			
			->get();
		
		if ($src->num_rows() == 1) {
			$pengguna = $src->row();
			$menu = $ci->pengguna_grup_model->menu($pengguna->id);
			$ci->session->set_userdata('pengguna', $pengguna);
			$ci->session->set_userdata('menu', $menu);
		}
	}
	
	$controller = strtolower($ci->uri->segment(1));
	$method = strtolower($ci->uri->segment(2));
	
	$uri = strtolower($ci->uri->uri_string());
	
	$uri_pengecualian = array (
		'pengguna/masuk',
		'pengguna/keluar',
		'options/getGrupPengguna'
	);
	
	$is_cek_session = false;
	$id=session_pengguna('id');
	$menuoleh=$ci->pengguna_grup_model->menu($id);
	$uri2="";
	if(!empty($method)){
		$uri2=$controller.'/'.$method;
	}else{
		$uri2=$controller;
	}
	$menu1=menulv1($uri2,session_pengguna('id_pengguna_grup'));
	// var_dump($controller.'/'.$method);
	// var_dump($menu1);
	if ($controller == '' or $controller == 'site') {
		if (session_pengguna('id')) {
			redirect(site_url('/dasboard'));
		}
	}
	else if (array_search($uri, $uri_pengecualian) !== false ) {
		$is_cek_session = false;
	}
	else if($menu1==0){
		if (session_pengguna('id')&&array_search($uri, $uri_pengecualian) !== false) {
			
			echo '<script>alert("Maaf, anda tidak diizinkan mengakses halaman ini")</script>';
            echo'<script>window.location.href="'.base_url().'dasboard";</script>';
		}

	}
	else {
		$is_cek_session = true;
	}

	if ($is_cek_session) {
		if ( ! session_pengguna('id')) {
			redirect(site_url());
		}
	}
}
