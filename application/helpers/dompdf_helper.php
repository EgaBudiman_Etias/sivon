<?php

require_once('dompdf/autoload.inc.php');

use Dompdf\Dompdf;
use Dompdf\Options;

function new_dompdf()
{
	$options = new Options();
	$options->set('isRemoteEnabled', true);
	
	return new Dompdf($options);
}
